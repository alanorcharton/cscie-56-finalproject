package edu.harvard.cscie56.finalproject

import grails.test.mixin.TestFor
import org.joda.time.LocalTime
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ScheduleTime)
class ScheduleTimeSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Shows a LocalTime in correct format"() {

        given:"A new shedTime instance"
        def t = new ScheduleTime(schedTime: new LocalTime(8,14,32))

        when: "the the getDisplayString is called"
        def display = t.toString()

        then: "the format is HH:mm"
        display == "08:14"
    }
}
