package edu.harvard.cscie56.finalproject

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.joda.time.LocalTime
import org.joda.time.Minutes
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PredictionService)
//@Mock(Prediction)
class PredictionServiceSpec extends Specification {

    def predictionData = '''<?xml version="1.0" encoding="utf-8" ?>
<body copyright="All data copyright AC Transit 2014.">
    <predictions agencyTitle="AC Transit" routeTitle="NL" routeTag="NL" stopTitle="Lake Park Av &amp; Lakeshore Av" stopTag="9902310">
        <direction title="To San Francisco">
            <prediction epochTime="1418513566035" seconds="355" minutes="5" isDeparture="false" dirTag="NL_1_1" vehicle="1435" block="124002" tripTag="3936759" />
            <prediction epochTime="1418515210739" seconds="1999" minutes="33" isDeparture="false" affectedByLayover="true" dirTag="NL_1_1" vehicle="1430" block="124001" tripTag="3936760" />
            <prediction epochTime="1418516924801" seconds="3714" minutes="61" isDeparture="false" affectedByLayover="true" dirTag="NL_1_1" vehicle="1441" block="124004" tripTag="3936761" />
        </direction>
        <message text="Protests, detours for all downtown Oakland stops. actransit.org for info." priority="Normal"/>
    </predictions>
    <predictions agencyTitle="AC Transit" routeTitle="18" routeTag="18" stopTitle="Park Blvd &amp; Glen Park Rd" stopTag="1017500">
        <direction title="To Montclair">
            <prediction epochTime="1418514728498" seconds="1517" minutes="25" isDeparture="false" dirTag="18_117_0" vehicle="1463" block="18002" tripTag="3931406" />
            <prediction epochTime="1418516300078" seconds="3089" minutes="51" isDeparture="false" dirTag="18_117_0" vehicle="1349" block="18005" tripTag="3931407" />
            <prediction epochTime="1418517330224" seconds="4119" minutes="68" isDeparture="false" affectedByLayover="true" dirTag="18_117_0" vehicle="1068" block="18007" tripTag="3931408" />
        </direction>
        <message text="Protests, detours for all downtown Oakland stops. actransit.org for info." priority="Normal"/>
    </predictions>
    <predictions agencyTitle="AC Transit" routeTitle="V" routeTag="V" stopTitle="Park Blvd &amp; Glen Park Rd" stopTag="1017500" dirTitleBecauseNoPredictions="To College Ave. &amp; Broadway">
        <message text="Service changes Sunday Dec. 7. See actransit.org or call 511 for info" priority="Normal"/>
    </predictions>
    <predictions agencyTitle="AC Transit" routeTitle="NL" routeTag="NL" stopTitle="Macarthur Blvd &amp; Park Blvd" stopTag="1015130">
        <direction title="To Eastmont Transit Center">
            <prediction epochTime="1418513700076" seconds="489" minutes="8" isDeparture="false" dirTag="NL_37_0" vehicle="1441" block="124004" tripTag="3936792" />
            <prediction epochTime="1418515576595" seconds="2365" minutes="39" isDeparture="false" affectedByLayover="true" dirTag="NL_37_0" vehicle="1438" block="124003" tripTag="3936793" />
            <prediction epochTime="1418517380192" seconds="4169" minutes="69" isDeparture="false" affectedByLayover="true" dirTag="NL_37_0" vehicle="1435" block="124002" tripTag="3936794" />
        </direction>
        <message text="Protests, detours for all downtown Oakland stops. actransit.org for info." priority="Normal"/>
    </predictions>
</body>'''

    def singlePredictionData = '''<?xml version="1.0" encoding="utf-8" ?>
<body copyright="All data copyright AC Transit 2014.">
    <predictions agencyTitle="AC Transit" routeTitle="NL" routeTag="NL" stopTitle="Lake Park Av &amp; Lakeshore Av" stopTag="9902310">
        <direction title="To San Francisco">
            <prediction epochTime="1418513370480" seconds="397" minutes="6" isDeparture="false" dirTag="NL_1_1" vehicle="1435" block="124002" tripTag="3936759" />
            <prediction epochTime="1418515210739" seconds="2237" minutes="37" isDeparture="false" affectedByLayover="true" dirTag="NL_1_1" vehicle="1430" block="124001" tripTag="3936760" />
            <prediction epochTime="1418516924801" seconds="3951" minutes="65" isDeparture="false" affectedByLayover="true" dirTag="NL_1_1" vehicle="1441" block="124004" tripTag="3936761" />
        </direction>
        <message text="Protests, detours for all downtown Oakland stops. actransit.org for info." priority="Normal"/>
    </predictions>
</body>'''

    def errorPrediction = '''<?xml version="1.0" encoding="utf-8" ?>
<body copyright="All data copyright AC Transit 2014.">
    <Error shouldRetry="false">  For agency=actransit stop s=1014246 is on none of the directions for r=B so cannot determine which stop to provide data for.</Error>
</body>'''

    def error2Prediction = '''<?xml version="1.0" encoding="utf-8" ?>
<body copyright="All data copyright agencies listed below and NextBus Inc 2014.">
    <Error shouldRetry="false">
  Agency parameter "a=actransitstops=NL|9902310" is not valid.
</Error>
</body>'''

    def setup() {
    }

    def cleanup() {
    }

    def "Create a single prediction for a particular stop"(){

        when:"Prediction is parsed"
        List<Prediction> p = service.generatePredictions(singlePredictionData)

        then:"Create 1 Prediction Object with expected property values"
        p.size() == 1
        p[0].stopTitle == "Lake Park Av & Lakeshore Av"
        p[0].stopTag == "9902310"
        p[0].directionTitle == "To San Francisco"
        p[0].predictions == ['6','37','65']

    }

    def "A stop that has no predictions should display no predictions"(){

        when:"Prediction is parsed"
        List<Prediction> p = service.generatePredictions(predictionData)
        List<String> predictions = []


        then:"Create 1 Prediction Object with expected property values"
        p.size() == 4
        def index
        p.eachWithIndex{ it, i ->
            if( it.stopTag == "1017500" && it.routeTag =="V"){
                predictions << it.predictions
                index = i
            }}
        predictions[0] == ["No predictions"]
        index != null
        p[index].directionTitle =="To College Ave. & Broadway"

        }



    def "Create prediction objects for routes"(){

        when:"Prediction data is Parsed"
        List<Prediction> p = service.generatePredictions(predictionData)
        List<String> predictions = []

        then:"Create 4 Prediction objects"
        p.size() == 4
        p.each{ it ->
            if( it.stopTag == "9902310"){
                predictions<< it.predictions
             }  }
        predictions[0] == ["5","33","61"]
    }

    def "Service should handle an error response by not"(){


    }





}
