package edu.harvard.cscie56.finalproject

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import org.joda.time.LocalTime
import org.joda.time.Minutes
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(WatchService)
@Mock([WatchStop,BusUser])
class WatchServiceSpec extends Specification {


    def "Valid Stops get saved and added to the user's watch list and direction is saved lower case"() {

        given: "A new user and profile in the database"
        def prof = new BusUserProfile(email: "bonnie@smith.com",fullName: "Bonnie Smith")
        def user = new BusUser(username: "bonnie", password: "busstop", profile: prof )
        user.save(failOnError: true)
        //stopTag = suppliedStopTag
        //stopTitle = suppliedStopTitle
        //direction = suppliedDirection
        //route = suppliedRoute
        //agency = suppliedAgency

        when: "A new stop is added to the users watch list"
        def watch = service.addWatchedStop(user,
                suppliedStopTag,
                suppliedStopTitle,
                suppliedDirection,
                suppliedRoute,
                suppliedAgency)


        then: "The Watch Stop gets added to the user"
        BusUser bu = BusUser.get(user.id)
        bu != null
        WatchStop watched  = bu.stopsWatched[0]
        watched != null
        watched.stopDirection == expectedDirection
        watch == "ok"



        where:

        suppliedStopTag | suppliedStopTitle | suppliedAgency | suppliedRoute | suppliedDirection | expectedDirection
        "300"           |"Fourth and King"  |   "actransit"  | "B"           | "EAST"            | "east"
        "300"           |"Fourth and King"  |   "actransit"  | "B"           | "WEST"            | "west"



    }

    def "An update to the Schedule time for a Watched Stop saves the change and generates an expected PreemptTime"(){

        given: "A user with a watched Stop with preemtMins set to 5"
        def prof = new BusUserProfile(email: "bonnie@smith.com",fullName: "Bonnie Smith")
        def user = new BusUser(username: "bonnie", password: "busstop", profile: prof )
        def watched = new WatchStop(stopTitle:"Lake Park Av & Lakeshore Av",stopTag: "9902310",stopDirection: "west",
                                    routeTag: "NL", agency: "actransit", preemptMins: new Minutes(5),notifyHandle: "email",
                                    notifyType: "email")
        //watched.save(failOnError: true,flush: true)
        user.addToStopsWatched(watched)
        user.save(failOnError: true, flush: true)

        when: "The user updates the schedule time"

        service.updateWatchedStopTime(user,watched.id, new LocalTime(7,35,00).getMillisOfDay())

        then: "The updated record can be retrieved and the preempt time is correct"
        def foundUser = BusUser.get(user.id)
        def targetStop
        def userStops = foundUser.stopsWatched
        for( ws in userStops){
            if(ws.id == watched.id)
                targetStop = ws
        }

        targetStop != null
        targetStop.preemptMins != null
        targetStop.preemptTime == new LocalTime(7,30,00).getMillisOfDay()

    }
}
