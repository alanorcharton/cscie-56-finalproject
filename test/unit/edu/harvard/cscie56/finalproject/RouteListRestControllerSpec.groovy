package edu.harvard.cscie56.finalproject

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(RouteListRestController)
@Mock([Route,RouteSegment])
class RouteListRestControllerSpec extends Specification {

    void "Requesting the RouteListRest controller to show a non existent Route"(){

        given:"A Route"
        Route r = new Route(routeTag: "B", routeTitle: "B", )
        r.save(failOnError: true, flush: true)
        params.id = 0


        when:"the show action is called with an invalid parameter"
        request.method = 'GET'
        response.format = 'json'
        controller.show()


        then:"an 404 error occurs"
        response.status == 404

    }

}
