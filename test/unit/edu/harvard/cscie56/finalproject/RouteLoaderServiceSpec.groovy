package edu.harvard.cscie56.finalproject

import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import org.joda.time.LocalTime
import org.joda.time.Minutes
import org.quartz.SchedulerConfigException
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(RouteLoaderService)
@Mock([RouteSegment, RoutePoint, Route, Stop, Direction, Schedule,ScheduleStop, ScheduleTime])
class RouteLoaderServiceSpec extends Specification {

    String routeData = ''' <body copyright="All data copyright AC Transit 2014.">
                                    <route tag="NL" title="NL" color="f69348" oppositeColor="000000" latMin="37.7691899" latMax="37.8251899" lonMin="-122.39362" lonMax="-122.17404">
                                        <stop tag="1001240" title="Thomas L Berkley Way (20th St)  Broadway" lat="37.8090799" lon="-122.2684" stopId="53888"/>
                                        <stop tag="1010650" title="Foothill Blvd &amp; Eastmont Transit Center" lat="37.76928" lon="-122.1741499" stopId="53449"/>
                                        <direction tag="NL_1_1" title="To San Francisco" name="West" useForUI="true">
                                        <stop tag="1001240"/>
                                        </direction>
                                        <direction tag="NL_37_0" title="To Eastmont Transit Center" name="East" useForUI="true">
                                        <stop tag="1010650"/>
                                        </direction>
                                        <path>
                                            <point lat="37.80931" lon="-122.26861"/>
                                            <point lat="37.80924" lon="-122.26866"/>
                                            <point lat="37.80948" lon="-122.26928"/>
                                        </path>
                                        <path>
                                        <point lat="37.77537" lon="-122.18514"/>
                                        <point lat="37.77531" lon="-122.18523"/>
                                        <point lat="37.77539" lon="-122.18532"/>
                                        </path>
                                    </route>
                                </body>'''


    String errorResponse = '''<?xml version="1.0" encoding="utf-8" ?>
                                <body copyright="All data copyright AC Transit 2014.">
                                <Error shouldRetry="false">
                                Could not get route "" for agency tag "actransit".
                                One of the tags could be bad.
                                </Error>
                                </body>'''




    def "Valid routes listings get saved and added to the database"(){



        given:"Results from a route listing"
        String routeList = ''' <body copyright="All data copyright AC Transit 2014.">
                                    <route tag="B" title="B"/>
                                    <route tag="BSD" title="Broadway Shuttle Weekdays"/>
                                    <route tag="BSN" title="Broadway Shuttle Fri/Sat Nights"/>
                                    <route tag="C" title="C"/>
                                    <route tag="CB" title="CB"/>
                                    <route tag="E" title="E"/>
                                    <route tag="F" title="F"/>
                                    <route tag="FS" title="FS"/>
                                </body>'''

        when:"A new routes are created by the service"
        service.loadBaseRouteData(routeList)

        then:"Eight Routes are created"
        Route.count() == 8
        Route.findByRouteTag("B").routeTitle == "B"
    }

    void "loadRouteSegment loads valid routePoints to the route segment and route"() {

        given: "Results form a call to get route data and a route without data"
        def route = new Route(routeTag: "NL", routeTitle: "NL")

        when: "A new route point is created by the service"
        service.loadRouteSegments(route, routeData)


        then: "One Route with Two RouteSegments are Created each with three points each"
        route.routeSegments.size() == 2
        route.routeSegments[0].routePoints.size() == 3
        route.routeSegments[1].routePoints.size() == 3

    }

    def "An existing route with route segments should not get any more segments loaded"(){

        given: "A Route that has route segment data"
        def route = new Route(routeTag: "B", routeTitle: "B")
        def rp1 = new RoutePoint(seqNum: 0, lat:"1.77537", lng: "-1.18514" )
        def rp2 = new RoutePoint(seqNum: 1, lat:"2.80948", lng: "-2.18514")
        def rs1 = new RouteSegment(segmentNumber: 0)
        def rs2 = new RouteSegment(segmentNumber: 1)
        rs1.addToRoutePoints(rp1)
        rs2.addToRoutePoints(rp2)
        route.addToRouteSegments(rs1)
        route.addToRouteSegments(rs2)
        route.save(failOnError: true)

        when: "loadRouteSegment is called to add more data"
        service.loadRouteSegments(route,routeData)

        then: "One Route with Two RouteSegments exist each with one point each"
        route.routeSegments.size() == 2
        route.routeSegments[0].routePoints.size() == 1
        route.routeSegments[1].routePoints.size() == 1



    }

    def "Stops and Directions are Added to the database"(){

        given: "A Route with no segment, stop, or direction data"
        def route = new Route(routeTag: "B", routeTitle: "B")

        when: "loadRouteSegment is called to add data"
        service.loadRouteSegments(route,routeData)

        then: "A direction and a two stops are added to the route"
        route.directions != null
        route.stops != null
        route.directions.size() == 2
        route.directions.collect{it.directionTag}.sort() == [ "NL_1_1","NL_37_0"]
        route.stops.size() == 2
        route.stops.collect{ it.stopTag }.sort() == ["1001240", "1010650"]
    }

    def "ScheduleStops and Schedule Times are read and added to the database"(){


        when:"the schedule is loaded from XMLstring"
        service.loadSchedule("B",scheduleData)

        then: "The schedule components can be loaded from the database"
        Schedule.count() == 2
        ScheduleStop.count() == 9
        ScheduleTime.count() == 44
        def mySchedule = Schedule.findByScheduleRouteAndScheduleDirection("B", "east")
        mySchedule.scheduleRoute == 'B'
        mySchedule.scheduleStops.size() == 5


    }


    def "Schedule added to the database and parameters confirmed east direction"(){


        when:"the schedule is loaded from XMLstring"
        service.loadSchedule("B",scheduleData)

        then: "The schedule components can be loaded from the database"
        Schedule.count() == 2
        def mySchedule = Schedule.findByScheduleRouteAndScheduleDirection("B","east")
        mySchedule.scheduleRoute == 'B'
        mySchedule.scheduleTitle == 'B'
        mySchedule.serviceClass == 'wkd'
        mySchedule.scheduleDirection == 'east'


    }

    def "Schedule times for a specific stop 1031300 west direction are confirmed"(){


        when:"the schedule is loaded from XMLstring"
        service.loadSchedule("B",scheduleData)

        then: "The schedule stop 1031300 west has 3 times and times are as expected"
        Schedule.count() == 2
        ScheduleTime.count() > 0
        def myStop = ScheduleStop.findBySchedStopTagAndSchedDirection("1031300","west")
        myStop.schedDepTimes.size() == 3
        def sortedTimes = myStop.schedDepTimes.collect{ it.getDisplayString() }.sort()
        sortedTimes == ["06:40", "07:46", "08:10"]


    }

    def "Test getting a Schedule for a particular stop"(){

        given:"A watched Stop"
        String stopTag = "1014240"

        when:"the schedule is loadef from XMLString and getScheduleForWatched Stop called"
        service.loadSchedule("B",scheduleData)
        ScheduleStop ss = service.getScheduleForWatchedStop(stopTag,"B","east")

        then:"Six times should be retrieved"
        ss != null
        ss.schedDepTimes.size() == 6

    }

    def "Test getting a Schedule for a  stop not on the schedule"(){

        given:"A watched Stop"
        String stopTag = "1000"

        when:"the schedule is loaded from XMLString and getScheduleForWatched Stop called"
        service.loadSchedule("B",scheduleData)
        ScheduleStop ss = service.getScheduleForWatchedStop(stopTag,"B","east")

        then:"Null times should be returned"
        ss == null

    }

    def "Test sending all null to the getScheduleForWatchStop method"(){

        given:"A watched Stop"
        String stopTag = "1000"

        when:"the schedule is loaded from XMLString and getScheduleForWatched Stop called"
        service.loadSchedule("B",scheduleData)
        ScheduleStop ss = service.getScheduleForWatchedStop(null,null,null)

        then:"Null should be returned"
        ss == null

    }


    def "loadRouteSegments is called with a null XMLParameter"(){

        given:"A selected route instance with only tag and title"
        Route r = new Route(routeTag: "B", routeTitle: "B", )

        when:"loadRouteSegment is called with null xml data"
        service.loadRouteSegments(r,null)

        then:"no Stop, RouteSegment, or Direction should be added to the database"
        Schedule.count() == 0
        Stop.count() == 0
        Direction.count() == 0
    }

    def "loadRouteSegments is called with a error message XMLParameter"(){

        given:"A selected route instance with only tag and title"
        Route r = new Route(routeTag: "B", routeTitle: "B", )

        when:"loadRouteSegment is called with null xml data"
        service.loadRouteSegments(r,errorResponse)

        then:"no Stop, RouteSegment, or Direction should be added to the database"
        Schedule.count() == 0
        Stop.count() == 0
        Direction.count() == 0
    }







    String scheduleData = '''<?xml version="1.0" encoding="utf-8" ?>
<body copyright="All data copyright AC Transit 2014.">
  <route tag="B" title="B" scheduleClass="1408FA" serviceClass="wkd" direction="East">
    <header>
      <stop tag="1410350">Transbay Temp Terminal</stop>
      <stop tag="301031300">Longridge Rd &amp; Lakeshore Av</stop>
      <stop tag="1031300">Longridge Rd &amp; Lakeshore Av</stop>
      <stop tag="1014240">Longridge Rd &amp; Grosvenor Pl</stop>
      <stop tag="301020770">Trestle Glen Rd &amp; Lake Shore Av (Wesleyway</stop>
    </header>
    <tr blockID="102002">
      <stop tag="1410350" epochTime="26700000">07:25:00</stop>
      <stop tag="301031300" epochTime="27780000">07:43:00</stop>
      <stop tag="1031300" epochTime="-1">--</stop>
      <stop tag="1014240" epochTime="-1">--</stop>
      <stop tag="301020770" epochTime="-1">--</stop>
    </tr>
    <tr blockID="102004">
      <stop tag="1410350" epochTime="59400000">16:30:00</stop>
      <stop tag="301031300" epochTime="-1">--</stop>
      <stop tag="1031300" epochTime="60480000">16:48:00</stop>
      <stop tag="1014240" epochTime="60780000">16:53:00</stop>
      <stop tag="301020770" epochTime="61200000">17:00:00</stop>
    </tr>
    <tr blockID="102001">
      <stop tag="1410350" epochTime="61800000">17:10:00</stop>
      <stop tag="301031300" epochTime="-1">--</stop>
      <stop tag="1031300" epochTime="63480000">17:38:00</stop>
      <stop tag="1014240" epochTime="63780000">17:43:00</stop>
      <stop tag="301020770" epochTime="64200000">17:50:00</stop>
    </tr>
    <tr blockID="102005">
      <stop tag="1410350" epochTime="63600000">17:40:00</stop>
      <stop tag="301031300" epochTime="-1">--</stop>
      <stop tag="1031300" epochTime="65100000">18:05:00</stop>
      <stop tag="1014240" epochTime="65400000">18:10:00</stop>
      <stop tag="301020770" epochTime="65820000">18:17:00</stop>
    </tr>
    <tr blockID="102004">
      <stop tag="1410350" epochTime="65400000">18:10:00</stop>
      <stop tag="301031300" epochTime="-1">--</stop>
      <stop tag="1031300" epochTime="66900000">18:35:00</stop>
      <stop tag="1014240" epochTime="67200000">18:40:00</stop>
      <stop tag="301020770" epochTime="67620000">18:47:00</stop>
    </tr>
    <tr blockID="102001">
      <stop tag="1410350" epochTime="66600000">18:30:00</stop>
      <stop tag="301031300" epochTime="-1">--</stop>
      <stop tag="1031300" epochTime="68100000">18:55:00</stop>
      <stop tag="1014240" epochTime="68400000">19:00:00</stop>
      <stop tag="301020770" epochTime="68820000">19:07:00</stop>
    </tr>
    <tr blockID="102005">
      <stop tag="1410350" epochTime="68100000">18:55:00</stop>
      <stop tag="301031300" epochTime="-1">--</stop>
      <stop tag="1031300" epochTime="69360000">19:16:00</stop>
      <stop tag="1014240" epochTime="69660000">19:21:00</stop>
      <stop tag="301020770" epochTime="70080000">19:28:00</stop>
    </tr>
  </route>
  <route tag="B" title="B" scheduleClass="1408FA" serviceClass="wkd" direction="West">
    <header>
      <stop tag="1031300">Longridge Rd &amp; Lakeshore Av</stop>
      <stop tag="1014240">Longridge Rd &amp; Grosvenor Pl</stop>
      <stop tag="1020770">Trestle Glen Rd &amp; Lake Shore Av (Wesleyway</stop>
      <stop tag="301410350">Transbay Temp Terminal</stop>
    </header>
    <tr blockID="102002">
      <stop tag="1031300" epochTime="24000000">06:40:00</stop>
      <stop tag="1014240" epochTime="24300000">06:45:00</stop>
      <stop tag="1020770" epochTime="24900000">06:55:00</stop>
      <stop tag="301410350" epochTime="26100000">07:15:00</stop>
    </tr>
    <tr blockID="102002">
      <stop tag="1031300" epochTime="27960000">07:46:00</stop>
      <stop tag="1014240" epochTime="28260000">07:51:00</stop>
      <stop tag="1020770" epochTime="28860000">08:01:00</stop>
      <stop tag="301410350" epochTime="30060000">08:21:00</stop>
    </tr>
    <tr blockID="102003">
      <stop tag="1031300" epochTime="29400000">08:10:00</stop>
      <stop tag="1014240" epochTime="29700000">08:15:00</stop>
      <stop tag="1020770" epochTime="30300000">08:25:00</stop>
      <stop tag="301410350" epochTime="31680000">08:48:00</stop>
    </tr>
    <tr blockID="102004">
      <stop tag="1031300" epochTime="-1">--</stop>
      <stop tag="1014240" epochTime="-1">--</stop>
      <stop tag="1020770" epochTime="62520000">17:22:00</stop>
      <stop tag="301410350" epochTime="63900000">17:45:00</stop>
    </tr>
    <tr blockID="102001">
      <stop tag="1031300" epochTime="-1">--</stop>
      <stop tag="1014240" epochTime="-1">--</stop>
      <stop tag="1020770" epochTime="64560000">17:56:00</stop>
      <stop tag="301410350" epochTime="65940000">18:19:00</stop>
    </tr>
    <tr blockID="102005">
      <stop tag="1031300" epochTime="-1">--</stop>
      <stop tag="1014240" epochTime="-1">--</stop>
      <stop tag="1020770" epochTime="66180000">18:23:00</stop>
      <stop tag="301410350" epochTime="67560000">18:46:00</stop>
    </tr>
  </route>
</body>
'''

}
