package edu.harvard.cscie56.finalproject

import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import org.joda.time.LocalTime
import org.joda.time.Minutes
import spock.lang.Specification
import grails.converters.JSON

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(RouteController)
@Mock([ScheduleStop, WatchStop, ScheduleTime, Schedule, Route, RouteSegment])
class RouteControllerSpec extends Specification {

    WatchStop wstop6 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag: "B", stopTag: "1014240",
            stopTitle: "Preempt 9:20", stopDirection: "east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency: "actransit", preemptTime: new LocalTime(9,20,00).getMillisOfDay())


    ScheduleStop stop1 = new ScheduleStop(schedStopTag: '301031300',
            schedDirection: 'WEST',
            schedRouteTag: 'B',
            schedStopTitle: 'Longridge Rd & Lakeshore Av'  )

    ScheduleTime time1 = new ScheduleTime(schedTime: new LocalTime(18,02,00))
    ScheduleTime time2 = new ScheduleTime(schedTime: new LocalTime(19,12,00))



    void "Getting a Schedule for a particular WatchStop "() {

        given: "a mock route loader service"
        JSON.registerObjectMarshaller(ScheduleStop) { ScheduleStop ss ->
            return [id           : ss.id,
                    stopTag      : ss.schedStopTag,
                    stopTitle    : ss.schedStopTitle,
                    direction    : ss.schedDirection,
                    scheduleTimes: ss.schedDepTimes]
        }

        JSON.registerObjectMarshaller(ScheduleTime) { ScheduleTime st ->
            return [time: st.getDisplayString(), timeinmillis: st.getTimeInMillis()]
        }
        wstop6.save(failOnError: true)
       stop1.addToSchedDepTimes(time1)
       stop1.addToSchedDepTimes(time2)
        stop1.save(failOnError: true)
        def mockRouteLoaderService = Mock(RouteLoaderService)
        1 * mockRouteLoaderService.getNextBusSchedule(_,_)
        1 * mockRouteLoaderService.loadSchedule(_,_)
        1 * mockRouteLoaderService.getScheduleForWatchedStop(_,_,_) >> stop1

        controller.routeLoaderService = mockRouteLoaderService

        when: "Controller action invoked"
        controller.getSchedule(1)

        then: "response is json with empty times"
        response.text == '{"stoptimes":{"deptimes":{"id":1,"stopTag":"301031300","stopTitle":"Longridge Rd & Lakeshore Av","direction":"west","scheduleTimes":[{"time":"18:02","timeinmillis":"64920000"},{"time":"19:12","timeinmillis":"69120000"}]},"notificationid":1}}'
    }

    void "Getting a Schedule for a  WatchStop that has no schedule "() {

        given: "a mock route loader service"
        JSON.registerObjectMarshaller(ScheduleStop) { ScheduleStop ss ->
            return [id           : ss.id,
                    stopTag      : ss.schedStopTag,
                    stopTitle    : ss.schedStopTitle,
                    direction    : ss.schedDirection,
                    scheduleTimes: ss.schedDepTimes]
        }

        JSON.registerObjectMarshaller(ScheduleTime) { ScheduleTime st ->
            return [time: st.getDisplayString(), timeinmillis: st.getTimeInMillis()]
        }
        wstop6.save(failOnError: true)
        stop1.addToSchedDepTimes(time1)
        stop1.addToSchedDepTimes(time2)
        stop1.save(failOnError: true)
        def mockRouteLoaderService = Mock(RouteLoaderService)
        1 * mockRouteLoaderService.getNextBusSchedule(_,_)
        1 * mockRouteLoaderService.loadSchedule(_,_)
        1 * mockRouteLoaderService.getScheduleForWatchedStop(_,_,_) >> null

        controller.routeLoaderService = mockRouteLoaderService

        when: "Controller action invoked"
        controller.getSchedule(1)

        then: "response is json with empty times"
        response.text == '{"stoptimes":{"deptimes":null,"notificationid":1}}'
    }


    void "Test the loadRoutes action when a route is not found"(){

        when: "Controller is invoked"
        controller.loadRoutes()

        then: "Redirection happens"
        response.redirectedUrl == '/routeListRest/show/0'
    }

}
