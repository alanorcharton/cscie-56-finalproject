package edu.harvard.cscie56.finalproject

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */

@TestFor(RouteRestController)
@Mock([Route,RouteSegment])
class RouteRestControllerSpec extends Specification {



    void "Requesting the RouteListRest controller to show a non existent Route"(){

    }
}
