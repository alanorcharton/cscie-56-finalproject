package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime

class ScheduleTimeIntegrationIntegrationSpec extends IntegrationSpec {

    def setup() {
    }

    def cleanup() {
    }

    void "Shows a LocalTime in correct format"() {

        given:"A new shedTime instance"
        def t = new ScheduleTime(schedTime: new LocalTime(8,14,32))

        when: "the the getDisplayString is called"
        def display = t.getDisplayString()//t.toString()

        then: "the format is HH:mm"
        display == "08:14"
    }

    def "Show a LocalTime in milliseconds of day"() {
        given:"A new ScheduleTime instance"
        def t = new ScheduleTime(schedTime: new LocalTime(6,05,00))

        when: "the getTimeInMillis is called"
        def display = t.getTimeInMillis()

        then:" the format is in milliseconds and matches expected result"
        display == "21900000"

    }

    def "Show a LocalTime created in milliseconds of day fromatted"() {
        given:"A new ScheduleTime instance"
        Long millis = new Long(23700000)
        def t = new ScheduleTime(schedTime: new LocalTime(millis,DateTimeZone.UTC))

        when: "the getTimeInMillis is called"
        def display = t.getDisplayString()

        then:" the format is in milliseconds and matches expected result"
        display == "06:35"


    }
}
