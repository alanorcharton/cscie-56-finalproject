package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec
import org.joda.time.*


class BusUserIntegrationIntegrationSpec extends IntegrationSpec {

    BusUser buser = new BusUser(username: 'brenda', password: 'ramsbottom')
    WatchStop wstop1 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
                                stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"west", preemptMins: new Minutes(5),
                                schedTime: new LocalTime(8,15,00), agency:"actransit", preemptTime: new LocalTime(8,10,00).getMillisOfDay())

    WatchStop wstop2 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(18,30,00).getMillisOfDay())




    void "Saving a BusUser to the database"() {

        given: "A new BusUser"
        def auser = new BusUser(username: 'frank', password: 'password1' )

        when: "The user is saved"
        auser.save()

        then: "the user saved and can be found in the database"
        auser.errors.errorCount == 0
        auser.id != null
        auser.get(auser.id).username == auser.username
    }

    def "Updating BusUser password changed the password property"(){

        given: "An existing user"
        def existingUser = new BusUser(username: 'frank', password: 'password1' )
        existingUser.save(failOnError: true)

        when: "A password is changed"
        def foundUser = BusUser.get(existingUser.id)
        foundUser.password = 'changed1'
        foundUser.save(failOnError: true)

        then: "the password is updated in the database"
        BusUser.get(existingUser.id).password == 'changed1'

    }

    def "Deleting an existing BusUser removes it from the database"(){

        given: "An existing bususer"
        def auser = new BusUser(username: 'frank', password: 'password1' )
        auser.save(failOnError: true)

        when:"The user is deleted"
        def foundUser = BusUser.get(auser.id)
        foundUser.delete(flush: true)

        then:"the Bus user is removed from the database"
        !BusUser.exists(foundUser.id)


    }

    def "Profile linked to a BusUser can be retrieved"(){

        given: "A user with a profile"
        def auser = new BusUser(username: 'frank', password: 'password1' )
        auser.profile = new BusUserProfile(email: "frank@daisy.com", twitterHandle: "bigFrank54", fullName: "frank smith")
        auser.save(failOnError: true)

        when: "the BusUser is retrived by the user id"
        def foundUser = BusUser.get(auser.id)

        then:"The profile properties can be retrieved"
        foundUser != null
        BusUserProfile.count() == 1
        foundUser.profile.email == 'frank@daisy.com'
        foundUser.profile.twitterHandle == 'bigFrank54'
        foundUser.profile.fullName == "frank smith"


    }

    def "Deleting an existing BusUser with a profile removes profile from the database"(){

        given: "An existing bususer"
        def auser = new BusUser(username: 'frank', password: 'password1' )
        auser.profile = new BusUserProfile(email: "frank@daisy.com", twitterHandle: "bigFrank54", fullName: "frank smith")
        auser.save(failOnError: true)

        when:"The user is deleted"
        def foundUser = BusUser.get(auser.id)
        foundUser.delete(flush: true)

        then:"the Bus user is removed from the database"
        !BusUser.exists(foundUser.id)
        BusUserProfile.count() == 0


    }

    def "Storing a BusUser with a Watched stop to the database"(){

        given: "A BusUser"
        buser.save(failOnError: true)

        when:"A BusUser with a watched stop is saved"
        wstop1.save(failOnError: true)
        buser.addToStopsWatched(wstop1)
        buser.save()

        then:"The user can be found in the data base with watched stop info"
        buser.errors.errorCount == 0
        buser.id != null
        buser.get(buser.id).username == 'brenda'
        WatchStop.count() == 1
        buser.get(buser.id).stopsWatched[0].notifyType == "twitter"
    }



    def "Deleting watchedStop form a BusUser with two Watched stops removes the watched stop"(){

        given: "A BusUser wite 2 Stops saved"
        buser.save(failOnError: true)
        wstop1.save(failOnError: true)
        wstop2.save(failOnError: true)
        buser.addToStopsWatched(wstop1)
        buser.addToStopsWatched(wstop2)
        buser.save()

        when:"A Watched stop is deleted"
        def foundUser = BusUser.get(buser.id)
        foundUser.removeFromStopsWatched(wstop1)

        then:"The user can be found in the data base with watched stop info"
        buser.errors.errorCount == 0
        buser.id != null
        buser.get(buser.id).username == 'brenda'
        WatchStop.count() == 2
        buser.get(buser.id).stopsWatched[0].stopDirection == 'east'
    }

}
