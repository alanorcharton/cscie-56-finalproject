package edu.harvard.cscie56.finalproject

import grails.gorm.DetachedCriteria
import grails.test.spock.IntegrationSpec
import org.joda.time.LocalTime
import org.joda.time.Minutes

class WatchStopIntegrationIntegrationSpec extends IntegrationSpec {


    WatchStop wstop1 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"The 6:11 bus", stopDirection:"west", preemptMins: new Minutes(5),
            schedTime: new LocalTime(8,15,00), agency:"actransit", preemptTime: new LocalTime(6,11,00).getMillisOfDay())

    WatchStop wstop2 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"The 6:10 bus", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,10,00).getMillisOfDay())


    WatchStop wstop3 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"The 6:12 bus", stopDirection:"west", preemptMins: new Minutes(5),
            schedTime: new LocalTime(8,15,00), agency:"actransit", preemptTime: new LocalTime(6,12,00).getMillisOfDay())

    WatchStop wstop4 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,13,00).getMillisOfDay())


    WatchStop wstop5 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag: "NL", stopTag: "9902310",
            stopTitle: "Preempt 6:10", stopDirection: "west", preemptMins: new Minutes(5),
            agency: "actransit", schedTime: new LocalTime(9,35,00), preemptTime: new LocalTime(6,10,00).getMillisOfDay())

    WatchStop wstop6 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag: "B", stopTag: "1014240",
            stopTitle: "Preempt 9:20", stopDirection: "east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency: "actransit", preemptTime: new LocalTime(9,20,00).getMillisOfDay())

    def "Retrieve a Watch stop from the database by time"() {


        given:"Four watches saved to the database"
        wstop1.save(failOnError: true)
        wstop2.save(failOnError: true)
        wstop3.save(failOnError: true)
        wstop4.save(failOnError: true)


        when:"Four watches are saved to the database"
        def myBusses = WatchStop.where {preemptTime in new LocalTime(6,10,00).getMillisOfDay()..new LocalTime(6,11,00).getMillisOfDay()}.list(sort: "stopTitle")
        def mins = myBusses.collect{ it.stopTitle}




        then:"Can pull the 6:10 and 6:11 preemptTime records from the database"
        //WatchStop.count() == 4
        mins == [ "The 6:10 bus", "The 6:11 bus"]


    }

    def "Retrieve another Watch stop from the database by time"() {


        given:"Four watches saved to the database"
        wstop5.save(failOnError: true)
        wstop6.save(failOnError: true)



        when:"Retrieved by time"
       // def criteria = new DetachedCriteria(WatchStop).build {
       //     eq 'preemptTime', LocalTime(6,10,00)
       // }
        //def myBusses = WatchStop.where { preemptTime == LocalTime(6,10,00) }.list(sort: "stopTitle")
        int time1 = new LocalTime(6,10,00).getMillisOfDay()
        int time2 = new LocalTime(9,20,00).getMillisOfDay()
        def myBusses = WatchStop.where { preemptTime in time1..time2}.list()
        def mins = myBusses.collect{ it.stopTitle}




        then:"Can pull the 6:10 and 6:11 preemptTime records from the database"
        myBusses.size() == 2
        mins == [ "Preempt 6:10", "Preempt 9:20"]


    }

    def "Retrieve a WatchStop record from the database and display in format for querying predictions"(){

        given: "A saved watch stop record"
        wstop4.save()

        when: "The record is retrieved"
        def chosen = WatchStop.get(wstop4.id)

        then: "The record can be displays in correct format"
        chosen.toMultiPredictString() == "B|1014240"

    }
}
