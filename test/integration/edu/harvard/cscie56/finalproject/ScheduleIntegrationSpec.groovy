package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec
import org.joda.time.LocalTime

class ScheduleIntegrationSpec extends IntegrationSpec {

    def setup() {
    }

    def cleanup() {
    }

    Schedule bSched = new Schedule(scheduleRoute: 'B', serviceClass: 'wkd',
                                   scheduleTitle: 'B', scheduleDirection: "WEST")



    ScheduleStop stop1 = new ScheduleStop(schedStopTag: '301031300',
            schedDirection: 'WEST',
            schedRouteTag: 'B',
            schedStopTitle: 'Longridge Rd & Lakeshore Av'  )


    ScheduleStop stop2 = new ScheduleStop(schedStopTag: '1014240',
            schedDirection: 'EAST',
            schedRouteTag: 'B',
            schedStopTitle: 'Longridge Rd & Grosvenor Pl'  )

    ScheduleTime time1 = new ScheduleTime(schedTime: new LocalTime(18,02,00))
    ScheduleTime time2 = new ScheduleTime(schedTime: new LocalTime(19,12,00))
    ScheduleTime time3 = new ScheduleTime(schedTime: new LocalTime(6,05,00))
    ScheduleTime time4 = new ScheduleTime(schedTime: new LocalTime(8,12,00))

    void "Save a Schedule to the database"() {

        given:"Stops and times added to the schedule"
        stop1.addToSchedDepTimes(time1)
        stop1.addToSchedDepTimes(time2)
        stop2.addToSchedDepTimes(time3)
        stop2.addToSchedDepTimes(time4)
        bSched.addToScheduleStops(stop1)
        bSched.addToScheduleStops(stop2)



        when:"Saved the schedule to the database"
        bSched.save(failOnError: true)

        then:"Schedule can be retrieved with 2 stops and 2 times each"
        bSched.id != null
        Schedule.count() == 1
        Schedule.get(bSched.id).scheduleDirection == "west"
        Schedule.get(bSched.id).scheduleStops.size() == 2
        Schedule.get(bSched.id).scheduleStops[0].schedDepTimes.size() == 2
        Schedule.get(bSched.id).scheduleStops[1].schedDepTimes.size() == 2


    }
}
