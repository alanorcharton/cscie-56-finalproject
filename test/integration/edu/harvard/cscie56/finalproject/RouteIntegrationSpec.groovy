package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec

class RouteIntegrationSpec extends IntegrationSpec {


    void "Saving a route to the database"() {

        given:"A new Route"
        def r = new Route(routeTag: "NL", routeTitle: "NL", routeColor: "f69348", routeLatMin: "37.7691899",
                          routeLatMax: "37.8251899", routeLngMin: "-122.39362", routeLngMax: "-122.17404", agency: "actransit")

        when:"The route is saved"
        r.save()

        then:"It is saved successfully and can be found in the database"
        r.errors.errorCount == 0
        r.id != null
        r.get(r.id).routeTag == "NL"
        r.get(r.id).routeTitle == "NL"
        r.get(r.id).routeColor == "f69348"
        r.get(r.id).routeLatMin == "37.7691899"
        r.get(r.id).routeLatMax == "37.8251899"
        r.get(r.id).routeLngMin == "-122.39362"
        r.get(r.id).routeLngMax == "-122.17404"
        r.get(r.id).agency == "actransit"

    }

    def "Saving a route with just the tag and title to the database"(){

        given:"A new route with just tag and title"
        def r = new Route(routeTag: "NL", routeTitle: "NL")

        when:"The route is saved"
        r.save()

        then:"It is saved successfully and can be found in the database"
        r.errors.errorCount == 0
        r.id != 0
        r.get(r.id).routeTitle == "NL"
        r.get(r.id).routeTag == "NL"

    }

    def "Ensure routesegments added to a route can be retrieved"() {

        given:"A couple of route segments and a route"
        def r = new Route(routeTag: "NL", routeTitle: "NL", routeColor: "f69348", routeLatMin: "37.7691899",
                routeLatMax: "37.8251899", routeLngMin: "-122.39362", routeLngMax: "-122.17404", agency: "actransit")
        def rs1 = new RouteSegment(segmentNumber: 0)
        rs1.addToRoutePoints(new RoutePoint(seqNum: 0, lat: "37.80931", lng: "-122.26861"))
        rs1.save(failOnError: true)
        def rs2 = new RouteSegment(segmentNumber: 1)
        rs2.addToRoutePoints(new RoutePoint(seqNum: 0, lat: "37.80924", lng: "-122.26866"))
        rs2.save(failOnError: true)
        r.addToRouteSegments(rs1)
        r.addToRouteSegments(rs2)
        r.save()


        when:"The route is retrieved by the id"
        def foundRoute = Route.get(r.id)
        def sortedSegments = foundRoute.routeSegments.sort{ it.segmentNumber }
        def sortedSegmentNumbers = []
        sortedSegments.each { sortedSegmentNumbers << it.segmentNumber}

        then:"The segment numbers can be retrieved"
        sortedSegmentNumbers == [ 0, 1]


    }
}
