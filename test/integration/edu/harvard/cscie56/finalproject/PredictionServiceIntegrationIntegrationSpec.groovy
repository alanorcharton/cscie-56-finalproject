package edu.harvard.cscie56.finalproject

import grails.test.mixin.TestFor
import grails.test.spock.IntegrationSpec
import org.joda.time.LocalTime
import org.joda.time.Minutes

//@TestFor(PredictionService)
class PredictionServiceIntegrationIntegrationSpec extends IntegrationSpec {

    def setup() {
    }

    def cleanup() {
    }
    def predictionService

    WatchStop wstop3 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"The 6:12 bus", stopDirection:"west", preemptMins: new Minutes(5),
            schedTime: new LocalTime(8,15,00), agency:"actransit", preemptTime: new LocalTime(6,12,00).getMillisOfDay())

    WatchStop wstop4 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014241",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,13,00).getMillisOfDay())

    WatchStop wstop5 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"The 6:12 bus", stopDirection:"west", preemptMins: new Minutes(5),
            schedTime: new LocalTime(8,15,00), agency:"actransit", preemptTime: new LocalTime(6,12,00).getMillisOfDay())

    WatchStop wstop6 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,13,00).getMillisOfDay())

    WatchStop wstop7 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014243",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

    WatchStop wstop8 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014244",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

    WatchStop wstop9 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014245",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

    WatchStop wstop10 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014246",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

    WatchStop wstop11 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014247",
            stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
            schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

    def "test building the prediction string"(){

        given:"Two watch stops within a target prediction saved to the database"
        wstop3.save(failOnError: true)
        wstop4.save(failOnError: true)

        when:"The two watch stops are retrieved by preempt  time and formatted"
        //WatchStop.where {preemptTime in new LocalTime(6,10,00)..new LocalTime(6,11,00)}.list(sort: "stopTitle")
        ArrayList<WatchStop> ws = WatchStop.where { preemptTime in new LocalTime(6,12,00).getMillisOfDay()..new LocalTime(6,13,00).getMillisOfDay() }.list()
        WatchMap wm = new WatchMap()
        wm.addWatchStops(ws)
        List<String> searchStrings = predictionService.buildPredictionString("actransit",wm)

        then:"The string is formatted as expected"
        searchStrings[0] == "&a=actransit&stops=B|1014241&stops=B|1014240"



    }

    def "test building the prediction string with duplicate watch stop route and tags"(){

        given:"Two watch stops within a target prediction saved to the database"
        wstop5.save(failOnError: true)
        wstop6.save(failOnError: true)

        when:"The two watch stops are retrieved by preempt  time and formatted"
        //WatchStop.where {preemptTime in new LocalTime(6,10,00)..new LocalTime(6,11,00)}.list(sort: "stopTitle")
        ArrayList<WatchStop> ws = WatchStop.where { preemptTime in new LocalTime(6,12,00).getMillisOfDay()..new LocalTime(6,13,00).getMillisOfDay() }.list()
        WatchMap wm = new WatchMap()
        wm.addWatchStops(ws)
        List<String> searchStrings = predictionService.buildPredictionString("actransit",wm)

        then:"The string is formatted as expected with the duplicate removed"
        searchStrings[0] == "&a=actransit&stops=B|1014240"



    }

    def "If the number of stops to predict is higher that the MULTISTOPLIMIT per request the request is broken into several strings"(){

        given:"5 Stops with different ID's are saved and the MULTISTOPLIMIT is set a 2"
        wstop7.save(failOnError: true)
        wstop8.save(failOnError: true)
        wstop9.save(failOnError: true)
        wstop10.save(failOnError: true)
        wstop11.save(failOnError: true)
        predictionService.MULTISTOPLIMIT = 2

        when:"The stops are retrieved by the preemptTime and made into reqest strings"
        ArrayList<WatchStop> ws = WatchStop.where { preemptTime in new LocalTime(6,20,00).getMillisOfDay()..new LocalTime(6,22,00).getMillisOfDay() }.list()
        WatchMap wm = new WatchMap()
        wm.addWatchStops(ws)
        List<String> searchStrings = predictionService.buildPredictionString("actransit",wm)

        then:"There are the strings created for the Multi stop prediction lookup"
        searchStrings.size() == 3
        searchStrings[0] == "&a=actransit&stops=B|1014247&stops=B|1014246"
        searchStrings[1] == "&a=actransit&stops=B|1014245&stops=B|1014244"
        searchStrings[2] == "&a=actransit&stops=B|1014243"

    }

    def "The correct number of Watch Stops are associated with a prediction key in the WatchMap"(){

        given:"Four saved WatchStops three for the same route/stop prediction key"
        wstop3.save(failOnError: true)
        wstop4.save(failOnError: true)
        wstop5.save(failOnError: true)
        wstop6.save(failOnError: true)

        when:"The watch map is build and loaded with the watch stops from the query"
        WatchMap wm = new WatchMap()
        ArrayList<WatchStop> ws = WatchStop.where { preemptTime in new LocalTime(6,12,00).getMillisOfDay()..new LocalTime(6,13,00).getMillisOfDay() }.list()
        wm.addWatchStops(ws)

        then:"The count should be four and the count for B|1014240 should be 3"
        wm.stopCount == 4
        def duplicatePredictions = wm.getWatchStops("B|1014240")
        duplicatePredictions.size() == 3
        def singlePredict = wm.getWatchStops("B|1014241")
        singlePredict.size() == 1


    }

    def "Test email addresses for a couple users watching stops from different agencies"(){

        given: "A couple users with watched stops from different agencies"
        WatchStop wstop20 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
                stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
                schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

        WatchStop wstop30 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"N", stopTag:"6997",
                stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
                schedTime: new LocalTime(18,35,00), agency:"sf-muni", preemptTime: new LocalTime(6,30,00).getMillisOfDay())

        WatchStop wstop40 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"B", stopTag:"1014240",
                stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
                schedTime: new LocalTime(18,35,00), agency:"actransit", preemptTime: new LocalTime(6,21,00).getMillisOfDay())

        WatchStop wstop50 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag:"N", stopTag:"6997",
                stopTitle:"Longridge Rd & Grosvenor Pl", stopDirection:"east", preemptMins: new Minutes(5),
                schedTime: new LocalTime(18,35,00), agency:"sf-muni", preemptTime: new LocalTime(6,30,00).getMillisOfDay())

        def chuck = new BusUser(
                username: "chuck_norris",
                password: "highkick",
                profile: new BusUserProfile(fullName: "Chuck Norris", email: "chuck_norris@gmail.com")
        )
        chuck.addToStopsWatched(wstop20)
        chuck.addToStopsWatched(wstop30)
        chuck.save(failOnError: true, flush: true)

        def robert = new BusUser(
                username: "rburns",
                password: "sausages",
                profile: new BusUserProfile(fullName: "Robert Burns", email: "robert_burns@gmail.com")
        )
        robert.addToStopsWatched(wstop40)
        robert.addToStopsWatched(wstop50)
        robert.save(failOnError: true, flush: true)


        when:"getMultiStopPredictions is called"
        def predictions = predictionService.getMultiStopPredictions(new LocalTime(6,20,00), new LocalTime(6,30,00))

        then:"emails come out"
        predictions.size() == 2
        predictions[0].emails.size() == 2
        predictions[1].emails.size() == 2
        predictions[0].emails.collect().sort() == ["chuck_norris@gmail.com","robert_burns@gmail.com"]
        predictions[1].emails.collect().sort() == ["chuck_norris@gmail.com","robert_burns@gmail.com"]

    }

    def "Test sending email to a users"(){

        given:"A prediction with 2 emails"
        def p1 = new Prediction(stopTag: "10101", routeTag: "Z",
                            emails: ["lastbuscscie56@gmail.com","lastbuscscie56@gmail.com"],
                            predictions: ["3","4"],stopTitle: "test stop")
        def pList = [p1]

        when:"Send Email is called"
        predictionService.sendEmails(pList)

        then:"Email is sent"
        p1.emails == ["lastbuscscie56@gmail.com","lastbuscscie56@gmail.com"]
    }

    def "Create a single prediction for a particular stop"(){

        when:"Prediction is parsed"
        List<Prediction> p = predictionService.generatePredictions(singlePredictionData)

        then:"Create 1 Prediction Object with expected property values"
        p.size() == 1
        p[0].stopTitle == "Lake Park Av & Lakeshore Av"
        p[0].stopTag == "9902310"
        p[0].directionTitle == "To San Francisco"

    }







    def singlePredictionData = '''<?xml version="1.0" encoding="utf-8" ?>
<body copyright="All data copyright AC Transit 2014.">
    <predictions agencyTitle="AC Transit" routeTitle="NL" routeTag="NL" stopTitle="Lake Park Av &amp; Lakeshore Av" stopTag="9902310">
        <direction title="To San Francisco">
            <prediction epochTime="1418513370480" seconds="397" minutes="6" isDeparture="false" dirTag="NL_1_1" vehicle="1435" block="124002" tripTag="3936759" />
            <prediction epochTime="1418515210739" seconds="2237" minutes="37" isDeparture="false" affectedByLayover="true" dirTag="NL_1_1" vehicle="1430" block="124001" tripTag="3936760" />
            <prediction epochTime="1418516924801" seconds="3951" minutes="65" isDeparture="false" affectedByLayover="true" dirTag="NL_1_1" vehicle="1441" block="124004" tripTag="3936761" />
        </direction>
        <message text="Protests, detours for all downtown Oakland stops. actransit.org for info." priority="Normal"/>
    </predictions>
</body>'''


}
