package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec
import org.joda.time.LocalTime

class ScheduleStopIntegrationIntegrationSpec extends IntegrationSpec {

    def setup() {
    }

    def cleanup() {
    }
    ScheduleStop stop1 = new ScheduleStop(schedStopTag: '301031300',
                                          schedDirection: 'WEST',
                                          schedRouteTag: 'B',
                                          schedStopTitle: 'Longridge Rd & Lakeshore Av'  )


    ScheduleStop stop2 = new ScheduleStop(schedStopTag: '1014240',
                                          schedDirection: 'EAST',
                                          schedRouteTag: 'B',
                                          schedStopTitle: 'Longridge Rd & Grosvenor Pl'  )

    ScheduleTime time1 = new ScheduleTime(schedTime: new LocalTime(18,02,00))
    ScheduleTime time2 = new ScheduleTime(schedTime: new LocalTime(19,12,00))
    ScheduleTime time3 = new ScheduleTime(schedTime: new LocalTime(6,05,00))
    ScheduleTime time4 = new ScheduleTime(schedTime: new LocalTime(8,12,00))


    def "Save a ScheduleStop to the database"() {

        when:"A schedule stop is saved"
        stop1.save()

        then:"The schedule stop can be retrieved from the database"
        stop1.id != null
        stop1.errors.errorCount == 0
        ScheduleStop.get(stop1.id).schedStopTag == '301031300'
        ScheduleStop.get(stop1.id).schedDirection == 'west'

    }

    def "Save a Schedule Stop with Sechedule Times to the database"(){

        when:"A schedule stop with times are saved"
        stop1.addToSchedDepTimes(time1)
        stop1.addToSchedDepTimes(time2)
        stop1.save(flush: true)

        then:"The schedule Stop has 2 schedule times associated"
        ScheduleTime.count() == 2
        def out = ScheduleStop.get(stop1.id).schedDepTimes.collect { it.toString() }.sort()
        out == ['18:02','19:12']

    }

    def "Save Schedule Stops in each direction with ScheduleTimes to the database"(){

        when:"A schedule stop with times are saved"
        stop1.addToSchedDepTimes(time1)
        stop1.addToSchedDepTimes(time2)
        stop1.save(flush: true)
        stop2.addToSchedDepTimes(time3)
        stop2.addToSchedDepTimes(time4)
        stop2.save()

        then:"The schedule Stop has 2 schedule times associated"
        ScheduleTime.count() == 4
        def westbound =ScheduleStop.findBySchedDirection('west')
        def outWest = westbound.schedDepTimes.collect { it.toString() }.sort()
        outWest == ['18:02','19:12']
        def eastbound =ScheduleStop.findBySchedDirection('east')
        def outEast = eastbound.schedDepTimes.collect { it.toString() }.sort()
        outEast == ['06:05','08:12']


    }
}
