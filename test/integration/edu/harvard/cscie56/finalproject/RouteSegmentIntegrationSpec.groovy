package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec

class RouteSegmentIntegrationSpec extends IntegrationSpec {



    def "Saving a route segment to the database"() {

        given: "A new route segment"
        def rs = new RouteSegment(segmentNumber: 2)

        when: "the segment is saved"
        rs.save()

        then: "it save successfully and can be found in the database"
        rs.errors.errorCount == 0
        rs.id != null
        rs.get(rs.id).dateCreated == rs.dateCreated
        rs.get(rs.id).segmentNumber == rs.segmentNumber

    }

    def "Ensure points added to a route segment can be retrieved"() {

        given: "A routeSegment with several points"
        def rs = new RouteSegment(segmentNumber: 3)
        rs.addToRoutePoints(new RoutePoint(seqNum: 0, lat: "37.80931", lng: "-122.26861"))
        rs.addToRoutePoints(new RoutePoint(seqNum: 1, lat: "37.80924", lng: "-122.26866"))
        rs.addToRoutePoints(new RoutePoint(seqNum: 2, lat: "37.80948", lng: "-122.26928"))
        rs.save(failOnError: true)

        when: "The route segment is retrieved by the id"
        def foundSegment = RouteSegment.get(rs.id)
        def resultList = []
        resultList = foundSegment.routePoints.sort{ a, b -> a.seqNum <=> b.seqNum }
        //foundSegment.routePoints.each{ point ->
        //        resultList[point.seqNum] = point
       // }


        then: "The latitude and longitude point data appear int he retrieved RouteSegments"
        resultList[0].seqNum == 0
        resultList[0].lat == "37.80931"
        resultList[0].lng == "-122.26861"
        resultList[1].seqNum == 1
        resultList[1].lat == "37.80924"
        resultList[1].lng == "-122.26866"
        resultList[2].seqNum == 2
        resultList[2].lat == "37.80948"
        resultList[2].lng == "-122.26928"


    }
}
