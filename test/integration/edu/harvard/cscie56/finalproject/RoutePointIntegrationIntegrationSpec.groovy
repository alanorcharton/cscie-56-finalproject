package edu.harvard.cscie56.finalproject

import grails.test.spock.IntegrationSpec

class RoutePointIntegrationIntegrationSpec extends IntegrationSpec {



    def "Saving a route point to the database"() {

        given: "A new route point"
        def pt1 = new RoutePoint(seqNum: 0, lat: "37.80931", lng: "-122.26861")

        when: "the point is saved"
        pt1.save()


        then: "it saved successfully and can be found in the database"
        pt1.errors.errorCount == 0
        pt1.id != null
        RoutePoint.get(pt1.id).seqNum == pt1.seqNum
        RoutePoint.get(pt1.id).lat == pt1.lat
        RoutePoint.get(pt1.id).lng == pt1.lng
    }
}
