package edu.harvard.cscie56.finalproject

import grails.transaction.Transactional
import org.joda.time.LocalTime

@Transactional
class PredictionService {

    def mailService
    public static MULTISTOPLIMIT = 150

    /**
     * Calls NextBus to get up to the MULTISTOP limit of predictions pre a particular agency
     * @param predictionStops
     * @return
     */
    def getMutiStopPredictionData(String predictionStops){


        String multiStopCmd = "http://webservices.nextbus.com/service/publicXMLFeed?command=predictionsForMultiStops${predictionStops}"
        log.debug("Command : ${multiStopCmd}")

        def predictionQuery = multiStopCmd.toURL()
        def predictionQueryText = predictionQuery.getText()
        return predictionQueryText
    }

    /**
     * Parses the prediction response and generates prediction Instances
     * @param predictionXML
     * @return
     */
    List<Prediction> generatePredictions(String predictionXML){
        def predict = new XmlSlurper().parseText(predictionXML)
        List<Prediction> allpredictions = []
        predict.predictions.each{ prediction ->
            log.debug("Prediction Title - > ${prediction.@agencyTitle}")
            Prediction p = new Prediction(agencyTitle: "${prediction.@agencyTitle}",
                                          stopTitle: "${prediction.@stopTitle}",
                                          routeTag: "${prediction.@routeTag}",
                                          stopTag: "${prediction.@stopTag}",
                                          )
            if(prediction.direction.size()){
                log.debug("Prediction for ${prediction.@routeTag} ")
                p.directionTitle = "${prediction.direction.@title}"
                if(prediction.direction.prediction){
                    def aprediction = prediction.direction.prediction
                    aprediction.each{ time ->
                        p.predictions << "${time.@minutes}"
                    }
                }

            } else{
                log.debug("***** NO Prediction for ${prediction.@routeTag} ")
                p.directionTitle = "${prediction.@dirTitleBecauseNoPredictions}"
                p.predictions << "No predictions"
            }

            allpredictions << p
        }

        return allpredictions

    }

    /**
     * Takes a time range and gets all the user Watch Stops with a preemptTime in that
     * range and generates a collection of predictions based on the stop data
     * @param lowerTime
     * @param upperTime
     * @return
     */
    List<Prediction> getMultiStopPredictions(LocalTime lowerTime, LocalTime upperTime) {

        def allPredictions = []

        def rateLimit = 150
        List<String> emails = []

        ArrayList<WatchStop> ws
        log.debug("There are ${WatchStop.count()} stops !!!!!")
        if (WatchStop.count() > 0) {
            ws = WatchStop.where { preemptTime in lowerTime.getMillisOfDay()..upperTime.getMillisOfDay() }.list()
            if (!ws) {
                log.debug("No watch stops in range")
                return allPredictions
            }

            WatchMap agencies = new WatchMap()
            agencies.addAgencyStops(ws)
            Set<String> uniqueAgencies = agencies.predictionKeys

            /* loop each unique agency and get the predictions strings for that agency */
            log.debug("Agency count : ${uniqueAgencies.size()}")
            for (ag in uniqueAgencies) {
                List<Prediction> agencyPredictions
                List<WatchStop> stopsForAgency = agencies.getWatchStops(ag)

                WatchMap predictionsMap = new WatchMap()
                predictionsMap.addWatchStops(stopsForAgency)
                List<String> predictionStrings = buildPredictionString(ag, predictionsMap)

                /* loop each predictionSting and get all the predictions each group of predictions */
                for (predictString in predictionStrings) {
                    String predictionXml = getMutiStopPredictionData(predictString)
                    agencyPredictions = generatePredictions(predictionXml)

                }

                /* consolidate predictions and associate email addresses */
                for (p in agencyPredictions) {
                    def watchStopsForPrediction = predictionsMap.getWatchStops(p.getPredictionKey())
                    watchStopsForPrediction.each { wsp ->
                        log.debug("each email ${wsp.user.profile.email}")
                        p.emails << wsp.user.profile.email }
                }
                allPredictions << agencyPredictions
            }
        }
        List<Prediction> consolidated = allPredictions.flatten()
        return consolidated
    }

    /**
     * Sends emails to users with their prediction data
     * @param allPredictions
     */
    def sendEmails(List<Prediction> allPredictions) {

        log.debug("IN email")
        for( prediction in allPredictions) {
            log.debug("Prediction email ${prediction.emails.toArray()}")
            mailService.sendMail {
                to prediction.emails.toArray()
                subject "LastBusPredictions"
                body "${prediction.toMessage()}"
            }

        }

    }



/**
 * Takes the agency string and a watchMap and creates an List of prediction strings. Handles the case
 * were there are more stops in the MultiPrediction string than is permitted as per the MULTISTOPLIMIT.
 * If there are more stops the string is broken down so that multiple calls can be made to the
 * nextBus web service
 * @param agency  transit agency
 * @param wm  the container WatchMap for all the watched stops
 * @return a list of strings for the multi stop prediction web lookup
 */
    List<String> buildPredictionString(String agency, WatchMap wm){

        log.debug("Count -> ${wm.stopCount}")
        def uniqueStopsSet = wm.getPredictionKeys()
        def uniqueStops = new ArrayList<String>(uniqueStopsSet)

        List<String> outerList = new ArrayList<String>()

        while (!uniqueStops.isEmpty()){
            def count = 0
            StringBuilder stopList = new StringBuilder("&a=${agency}")
            while ( (!uniqueStops.isEmpty()) && count < MULTISTOPLIMIT ){
                stopList << "&stops=${uniqueStops.pop()}"
                count++
            }
            outerList << stopList.toString()
        }
        return outerList

    }


}
