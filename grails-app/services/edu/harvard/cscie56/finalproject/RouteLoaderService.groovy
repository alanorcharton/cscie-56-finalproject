package edu.harvard.cscie56.finalproject

import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime

@Transactional
class RouteLoaderService {

    def serviceMethod() {

    }

    Boolean isLoaded() {

        if(RouteSegment.count())
            return true
        else
            return false

    }

    /**
     * Helper function to make calls to NextBus
     * @param request
     * @return
     */
    private String makeCalltoNextBus(URL request) {
        String response = null
        try {
            response = request.getText()
        } catch (Exception e) {
            log.error("Connection Problem : ${e.message}")
        }
        return response
    }

    /**
     * Call NextBus service to get all routes for the provided agency
     * @param agency
     * @return the webservice XMLresponse
     */
    String getRouteList(String agency) {
        String theAgency = agency
        def url = "http://webservices.nextbus.com/service/publicXMLFeed?command=routeList&a=${theAgency}"
        def routesUrl = url.toURL()
        String routesText = makeCalltoNextBus(routesUrl)
       // def routesText = routesUrl.getText()
        log.debug(routesText)
        return routesText

    }

    /**
     * Retrieves all the routes
     * @return
     */
    def List<Route> getAllRoutes(){
        return Route.list()
    }

    /**
     * Generates Route instances based on the route information for initial routelist query
     * @param routeListXML
     */
    def loadBaseRouteData(String routeListXML) {

        if(routeListXML) {
            def routesList = new XmlSlurper().parseText(routeListXML)
            for (route in routesList.route) {
                def r = new Route(routeTag: "${route.@tag}", routeTitle: "${route.@title}")
                r.validate()
                r.save()
            }
        }
    }

    /**
     * Calls NextBus to get the details of a specific route segments including path taken
     * @param routeTag
     * @return
     */
    String getRouteData(String routeTag) {
            String nextBusRoute = "http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=actransit&r=${routeTag}"
            log.debug("Route data url : ${nextBusRoute}")

            def routeNL = nextBusRoute.toURL()
           // def routeNLtext = routeNL.getText()
            def routeNLtext = makeCalltoNextBus(routeNL)
        return routeNLtext

    }

    /**
     * Parses the web service response for route details.
     * Loads the detail data of a specific route into the database
     * @param selectedRoute
     * @param routeXML
     * @return
     */
    def loadRouteSegments(Route selectedRoute, String routeXML) {

        if(!routeXML){
            log.debug("No webservice response sent to loadRouteSegments")
            return
        }

        if(selectedRoute){

            if(!selectedRoute.routeSegments){
                def myroute = new XmlSlurper().parseText(routeXML)

                selectedRoute.routeColor = myroute.route.@color
                selectedRoute.routeLatMin = myroute.route.@latMin
                selectedRoute.routeLatMax = myroute.route.@latMax
                selectedRoute.routeLngMin = myroute.route.@lonMin
                selectedRoute.routeLngMax = myroute.route.@lonMax
                //myroute.route.@lonMax

                myroute.route.stop.each{ routeStop ->
                    def rStop = new Stop(stopTag: "${routeStop.@tag}", stopId: "${routeStop.@stopId}",
                                        lat: "${routeStop.@lat}", lng: "${routeStop.@lon}", stopTitle: "${routeStop.@title}" )

                    rStop.validate()
                    rStop.save(failOnError: true)
                    selectedRoute.addToStops(rStop)

                }

                log.debug("Route segment Direction creation ")
                myroute.route.direction.each{ routeDirection ->
                    def rDirection = new Direction(directionTag: "${routeDirection.@tag}", directionTitle: "${routeDirection.@title}",
                                                   directionName: "${routeDirection.@name}")

                    routeDirection.stop.each{ dirStop ->
                        log.debug("processing stops for direction ${dirStop.@tag}")
                        def stop = Stop.findByStopTag("${dirStop.@tag}")
                        if(stop){
                            rDirection.addToStops(stop)
                        }
                    }

                    rDirection.validate()
                    selectedRoute.addToDirections(rDirection)
                    rDirection.save(failOnError: true)
                    selectedRoute.save(failOnError: true)
                }
                log.debug("Directions Done ")




                myroute.route.path.eachWithIndex { routePath, count ->

                    def rPath = new RouteSegment(segmentNumber: count)

                    myroute.route.path[count].point.eachWithIndex { point, ptSeqNum ->
                        def rPoint = new RoutePoint(seqNum: ptSeqNum, lat: "${point.@lat}", lng: "${point.@lon}")
                        rPoint.validate()
                        rPoint.save(failOnError: true)
                        rPath.addToRoutePoints(rPoint)
                        rPath.save(failOnError: true)
                    }

                    selectedRoute.addToRouteSegments(rPath)
                    selectedRoute.save(failOnError: true)
                    log.debug("Route data segment count : ${count}")
                }

            } else {
                log.debug("In loadRouteSegments. The selectedRoute already has data")
            }


        } else {
            log.warn("In loadRouteSegments. The selectedRoute object did not exist!")
        }

    }




    /**
     * Calls nextBus to obtain schedule information for a specific route
     * @param routeTag
     * @param theAgency
     * @return
     */
    String getNextBusSchedule(String routeTag, String theAgency){

        log.debug("In Schedule Loader:" + theAgency)
        def url = "http://webservices.nextbus.com/service/publicXMLFeed?command=schedule&a=${theAgency}&r=${routeTag}"
        def schedUrl = url.toURL()
        String schedText = makeCalltoNextBus(schedUrl)
        return schedText
    }


    /**
     * Finds the Schedule stop times for a particular Stop Tag on a
     * supplied route and direction
     * @return
     */
    ScheduleStop getScheduleForWatchedStop(String stopTag, String routeTag, String direction){

        log.debug("paramaters ${stopTag}, ${routeTag}, ${direction}")
        //get the schedule for the watched stop
        ScheduleStop targetStop = null
        Schedule sched = Schedule.findByScheduleRouteAndScheduleDirection(routeTag, direction)
        if(sched) {
            for (stop in sched.scheduleStops) {
                log.debug("Looking for match - Scheduled Stop ${stop.schedStopTag} the stop tag ${stopTag}")
                if (stop.schedStopTag == stopTag) {
                    log.debug(" Stop MATCHES a schedule stop")
                    targetStop = stop
                    break
                }
            }
        }
        log.debug("the target stop if found is: ${targetStop}")

        return targetStop
    }

    /**
     * Extracts Schedule information for a given route and loads in into the database
     *
     * @param schedRoute the Route name
     * @param scheduleXML the Schedule data from NextBus in XML format
     * @return
     */
    def loadSchedule(String schedRoute, String scheduleXML){

       def sched
       if(scheduleXML) {
           sched = new XmlSlurper().parseText(scheduleXML)
       }
       if(sched) {
           //Only load if the schedule does not already exist in db
           if(!Schedule.findByScheduleRoute(schedRoute)) {
               Schedule mySchedule


               //Schedule for each direction of route
               sched.route.eachWithIndex { dir, i ->
                   String direction = dir.@direction
                   String route = dir.@tag
                   String title = dir.@title
                   String sclass = dir.@serviceClass

                   mySchedule = new Schedule(scheduleRoute: schedRoute, scheduleTitle: title,
                               serviceClass: sclass, scheduleDirection: direction)

                       //each stop by direction
                       dir.header.stop.each { hst ->
                       ScheduleStop ss = new ScheduleStop(schedRouteTag: route, schedStopTitle: "$hst",
                               schedStopTag:"${hst.@tag}", schedDirection: direction)

                       //each stop departure time by direction
                       dir.tr.stop.each { trst ->
                           //match the stop tags
                           if (trst.@tag == hst.@tag) {
                               //check that a departure time exists
                               String scheduledTime = trst.@epochTime
                               Long scheduleTimeMilis = new Long(scheduledTime.toLong())
                               if (scheduledTime != "-1") {
                                   ss.addToSchedDepTimes(new ScheduleTime(schedTime: new LocalTime(scheduleTimeMilis,
                                           DateTimeZone.UTC)))

                               }
                           }
                           ss.validate()
                           ss.save(failOnError: true)
                           mySchedule.addToScheduleStops(ss)
                       }
                   }

                   mySchedule.validate()
                   mySchedule.save(failOnError: true)
               }
           }
       }
    }
}
