package edu.harvard.cscie56.finalproject

import grails.transaction.Transactional
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime
import org.joda.time.Minutes

class WatchException extends RuntimeException {
    String message
    WatchStop watchedStop
}

@Transactional
class WatchService {

    def messageSource



    /**
     * Adds a new bus stop information and creates a new WatchStop
     * A user has to be logged in to add a stop
     * @return response string
     */
    String addWatchedStop(BusUser currentUser,String stopTag, String stopTitle, String direction, String route, String agency) {

        String response

        if(currentUser) {

            WatchStop addStop = new WatchStop(notifyHandle: currentUser.profile.email,
                        notifyType: "email",
                        routeTag: route,
                        stopTitle: stopTitle,
                        stopDirection: direction.toLowerCase(),
                        stopTag: stopTag,
                        preemptMins: new Minutes(5),
                        schedTime: null,
                        agency: agency)

            currentUser.addToStopsWatched(addStop)

            currentUser.save(failOnError: true, flush: true)
            response = "ok"


        }else {
            response = messageSource.getMessage("default.insufficient.access.message",null,Locale.default)//"Please Log in to use this feature!"
            log.debug("addWatchedStop access error ${response}")
        }

        return response
    }

    /**
     * Updates the preemptMins. This is the number of minutes before the scheduled departure time
     * that the prediction service should check for a prediction
     * @param watchStopId
     * @param mins
     * @return the preempt mins value on success, otherwise 0
     */
    Integer updatePreemptMins(long watchStopId, int mins){
        def result = 0
        log.info("in update mins parameters ${watchStopId} and ${mins}")
        WatchStop ws = WatchStop.get(watchStopId)
        if(ws){


            Minutes preMins = new Minutes(mins)
            LocalTime preemptUpdate = ws.schedTime?.minusMinutes(preMins?.getMinutes())
            if(preemptUpdate) {
                ws.preemptTime = preemptUpdate.getMillisOfDay()
                ws.preemptMins = preMins

                ws.validate()
                ws.save(flush: true)
                log.debug("saved ${mins}")
                result = mins
            }
        }
        return result
    }

    /**
     * Deletes the Watched Stop
     * @param watchStopId
     */
    void deleteWatchStop(long watchStopId){

        WatchStop.load(watchStopId).delete()

    }

    /**
     * Updates the WatchStop preemptTime based on a change to Schedule time for a particular users stop
     * PreemptTime is the time before the scheduled arrival that the
     * user wants to be alerted
     *
     * @param currentUser
     * @param stopChangedId
     * @param updatedTime
     * @return the updated time in mins or an 0 on error
     */
    Integer updateWatchedStopTime(BusUser currentUser, Long stopChangedId, Integer updatedTime) {


        def userStops = currentUser.stopsWatched
        WatchStop targetRecord
        for (WatchStop ws in userStops){
            log.debug("stop id : ${stopChangedId} , iteration id : ${ws.id}")
            if( ws.id == stopChangedId){
                targetRecord = ws
            }
        }

        log.info("in the updateWatchStopTime service")
        if (targetRecord) {

            LocalTime schedUpdate = new LocalTime(updatedTime, DateTimeZone.UTC)
            Minutes preMins = targetRecord.preemptMins
            LocalTime preemptUpdate = schedUpdate?.minusMinutes(preMins?.getMinutes())


            targetRecord.schedTime = schedUpdate
            targetRecord.preemptTime = preemptUpdate.getMillisOfDay()

            currentUser.save(failOnError: true, flush: true)
            return updatedTime

        } else{
            return 0
        }

    }

}
