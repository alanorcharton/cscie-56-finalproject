package edu.harvard.cscie56.finalproject

import org.joda.time.LocalTime


class MonitorPredictionsJob {

    def predictionService
    static triggers = {
      //simple repeatInterval: 5000l // execute job once in 5 seconds
        simple name: 'busWatcherTrigger',
                startDelay: 60000l,
                repeatInterval: 60 * 1000
    }

    def execute() {
        // execute job

        log.debug("Running Job")
        LocalTime now = new LocalTime()
        LocalTime endtime = now.plusMinutes(1)
        log.debug("Now ${now}, End ${endtime}")
        def predictions = predictionService.getMultiStopPredictions(now, endtime)
        predictionService.sendEmails(predictions)

    }
}
