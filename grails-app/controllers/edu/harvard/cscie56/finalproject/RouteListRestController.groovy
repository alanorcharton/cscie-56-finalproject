package edu.harvard.cscie56.finalproject

import grails.rest.RestfulController

class RouteListRestController extends RestfulController{

    static responseFormats = ["json"]

    //readOnly
    RouteListRestController(){
        super(Route, true)
    }


}
