package edu.harvard.cscie56.finalproject

class RouteController {

    def routeLoaderService
    def springSecurityService
    def watchService
    def predictionService
    def messageSource

    /**
     * Entry point. Loads the base routes for an agency for the routes menu
     *
     */
    def index() {

            def routeCount = Route.count()
            log.info("Route Load Count ${routeCount}")

            if(routeCount == 0) {
                String routeListing = routeLoaderService.getRouteList("actransit")
                routeLoaderService.loadBaseRouteData(routeListing)
            }
            List<Route> routesForAgency = routeLoaderService.getAllRoutes()

        render view: "testroute" , model:[routes:routesForAgency,
                                          currentUser: springSecurityService.currentUser]
    }

    /**
     * Loads the detailed route data and returns the data as a JSON Response
     * If the data is already loaded skips the web service call
     * @return
     */
    def loadRoutes() {

        def routeTag = params.id
        def routeRecordId = 0

        def selectedRoute = Route.findByRouteTag(routeTag)
        if(selectedRoute){
            routeRecordId = selectedRoute.id
            if(!selectedRoute.routeSegments){
                log.info("Initial load of Route Segments : ${selectedRoute.routeTag}")

                String routeData = routeLoaderService.getRouteData(routeTag)
                routeLoaderService.loadRouteSegments(selectedRoute, routeData)
            }

        }
        else{

           log.debug("In load routes the selected route not found for routeTag ${routeTag}")


           // def route = new Route(routeTag: routeTag) 404 error ??

        }
        //Forward to provide REST response to client
        redirect action: 'show', controller: 'routeListRest', id: routeRecordId
    }

    //TODO:does this always fire - may only need to be conditional
    /**
     * Loads the schedule for a specific agency route
     * @param routeTag
     * @param agency
     * @return
     */
    /*
    def loadSchedule(String routeTag, String agency) {

        def schedule = routeLoaderService.getNextBusSchedule(routeTag,agency)
        routeLoaderService.loadSchedule(routeTag,schedule)

    }
    */

    /**
    *  Updates the current users Watch list. If the current user is not known
    *  or logged in then sends an error message.Permitting non-logged in users
    *  to call this
    */
    def updateWatch(String stop, String title, String direction, String route,String agency) {

        def response

        if (isLoggedIn()) {
            BusUser current = BusUser.get(springSecurityService.principal.id)
            response = watchService.addWatchedStop(current,stop,title,direction,route,agency)

        }else {

            response =  messageSource.getMessage("default.insufficient.access.message",null,Locale.default)
            log.debug("In updateWatch action ${response}")
        }

        render(contentType: "application/json") {
            updateresponse(response: response)
        }

    }

    /**
    * Retrieves the schedule for a specific stop.
    * @param id the WatchStop record identifier
    * @return JSON the scheduled departure times for a stop
    */
    def getSchedule(Long id) {

        ScheduleStop targetStop  = null
        Long watchStopId = id
        if(watchStopId) {
            WatchStop ws = WatchStop.get(watchStopId)

            if(ws) {
                def routeTag = ws.routeTag
                def agency = ws.agency
                def direction = ws.stopDirection
                String stopTag = ws.stopTag
                log.debug("Info on the schedule: routeTag: ${routeTag}, agency:${agency}, direction:${direction}")

                if(!Schedule.findByScheduleRoute(routeTag)) {
                    def schedule = routeLoaderService.getNextBusSchedule(routeTag, agency)
                    routeLoaderService.loadSchedule(routeTag, schedule)
                }
                targetStop = routeLoaderService.getScheduleForWatchedStop(stopTag, routeTag, direction)
            }

        }

        render(contentType: "application/json") {
                stoptimes(deptimes:targetStop, notificationid: watchStopId)
        }

    }

}