package edu.harvard.cscie56.finalproject

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime
import org.joda.time.Minutes
import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.security.web.WebAttributes

class BusUserController {

    def springSecurityService
    def watchService
//TODO: set default action
    def index() {}

    def logon() {


    }

    def authfail() {

        String msg = ''

        def exception = session[WebAttributes.AUTHENTICATION_EXCEPTION]
        if (exception) {
            if (exception instanceof AccountExpiredException) {
                msg = g.message(code: "springSecurity.errors.login.expired")
            } else if (exception instanceof CredentialsExpiredException) {
                msg = g.message(code: "springSecurity.errors.login.passwordExpired")
            } else if (exception instanceof DisabledException) {
                msg = g.message(code: "springSecurity.errors.login.disabled")
            } else if (exception instanceof LockedException) {
                msg = g.message(code: "springSecurity.errors.login.locked")
            } else {
                msg = g.message(code: "springSecurity.errors.login.fail")
            }
        }

        if (springSecurityService.isAjax(request)) {
            render([error: msg] as JSON)
        } else {
            flash.message = msg
            redirect action: 'logon', params: params
        }
    }

    /**
     * BusUser registration controller.
     */
    def register(){
        if(request.method == "POST"){
            def user = new BusUser(params)

            if (user.validate()){
                def userRole = BusRole.findByAuthority('ROLE_USER') ?: new BusRole(authority: 'ROLE_USER').save(failOnError: true)
                def adminRole = BusRole.findByAuthority('ROLE_ADMIN') ?: new BusRole(authority: 'ROLE_ADMIN').save(failOnError: true)
                user.save(failOnError: true, flush: true)
                BusUserBusRole.create user, userRole, true

                flash.message = "Successfully Created User"
                redirect action: 'logon', params: params
            }else{
                flash.message = "Error Registering User"
                return [user:user]
            }
        }
    }

    /**
     * Delete a Watched Stop
     * Only logged in users can delete their own records
     * @param deleteWatchStop
     * @return
     */
    @Secured(['ROLE_USER'])
    def deleteWatchRecord(long deleteWatchStop){

        log.debug("Request to delete a WatchStop record by a user record number:" + deleteWatchStop)
        watchService.deleteWatchStop(deleteWatchStop)

        redirect action:"stops"
    }

    /**
     * If a user selects a scheduled bus time to monitor. This controller is called.
     * Updates the users WatchStop - to add the scheduled time of the bus and create a
     * preempt time.
     * @return JSON Response
     */
    @Secured(['ROLE_USER'])
    def updateWatchTime(Long id, Integer updateWatchTime) {

        def response

            BusUser current = BusUser.get(springSecurityService.principal.id)
            log.debug("For the watch update ->" + current.username)
            response = watchService.updateWatchedStopTime(current,id,updateWatchTime)
        render(contentType: "application/json") {
            updateresponse(response: updatedTime)

        }

    }

    /**
     * Called when the user requests a change to the preempt Mins.
     * This is the minutes before the schedule time that they should be
     * Alerted.
     * @param watchStopId
     * @param preemptTime
     * @return JSON response updatedMins other than 0 confirms update
     */
    @Secured(['ROLE_USER'])
    def updatePreemptMins(long watchStopId, int preemptTime){

        log.debug("Here are the params for update mins : ${params}" )
        def updatedMins = watchService.updatePreemptMins(watchStopId,preemptTime)


        render(contentType: "application/json") {

            updateResult(update: updatedMins)

        }
    }
    /**
     * The stops action displays the users Watched Stops
     * User has to be logged in to see this
     * @return
     */
    @Secured(['ROLE_USER'])
    def stops() {

        BusUser auser = springSecurityService.currentUser
        def watchlist = auser.stopsWatched

        log.info("Rendering stops for user")
        def options = [ '2','5','10','15'];

        render view: 'stops', model: [user: auser, preemptOptions: options ]

    }
}
