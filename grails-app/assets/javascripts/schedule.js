/**
 * Created by orchie on 12/6/14.
 */
var pageSpecific = function() {
    console.log("Page Specific");
    $('#directionButtons a').hide();
    $('#routes').hide();


    $("[id^=deleteButton-]").click(function (data){
        var stopRecord = $(this).attr('id').replace(/deleteButton-/,'');
        $.post(this.href,{ deleteWatchStop: stopRecord}, function(data) { location.reload(); return false;});
            return false;

    });



    $("[id^=watchedMins] a").click(function (data){

        $(this).toggleClass('active');
    });

    $("[id^=minsList] a").click(function (data){

        var mbutton = $(this).attr('id').replace(/minItem-[0-9]+-/,'');

        //call to update the preempt Mins
        var preemptUpdate = this.innerHTML
        $.post(this.href,{ preemptTime: preemptUpdate, watchStopId: mbutton}, function(resp) {
                if (resp.updateResult.update != 0) {
                    $('#minsButton-' + mbutton).html(preemptUpdate + ' Mins' + ' <span class="caret"></span>')
                    return false;
                }
            }
        );
        $('#minsButton-' + mbutton).dropdown("toggle");
        return false;
    });

    //set event handler for all the Button links for schedule time dropdowns
    $("[id^=watchedStop] a").click(function (data){

        var sbutton = this.id;
        var stopnum = $(this).attr('id').replace(/schedButton-/,'');
        console.log("StopNum", stopnum);

        //Only make ajax call for times if needed
        //calls the getSchedule action in the Route Controller
        if ($('#schedList-' + stopnum).children().length == 0) {

            $.ajax({
                type: 'GET',
                url: this.href,
                beforeSend: function (){ $('#'+sbutton).toggleClass('active');},
                complete: function() { $('#'+sbutton).toggleClass('active');},
                success: function (stopinfo) {

                    var watchRecordId = stopinfo.stoptimes.notificationid;

                    if(stopinfo.stoptimes.deptimes == null) {

                        $('#schedButton-' + watchRecordId).html("None Available");
                        $('#'+sbutton).dropdown("toggle");

                        return false; }
                    $(stopinfo.stoptimes.deptimes.scheduleTimes).each(function (i, item) {

                        //Add the schedule times to the dropdown
                      $('#schedList-' + stopnum).append('<li><a href="' +
                          '../busUser/updateWatchTime/' + watchRecordId + '"' +
                          'id="millis-' + item.timeinmillis + '"' + '>'
                            + item.time + '</a></li>');
                    });

                    //Add a click handler to all the schedule times
                    //Posts the update to the updateWatchTime in the BusUserController
                    $('#schedList-' + stopnum + ' a').click(function() {
                        var buttontext = this.innerHTML;
                        var millis = $(this).attr('id').replace(/millis-/,'');

                        //Post the select time to save the user choice
                        $.post(this.href,{ updateWatchTime : millis },
                            function(data){
                                //--> check for a good response
                                var schedButton =$('#'+sbutton);
                                schedButton.html('<span id="spinner-' + stopnum + '"' +
                                    ' class="spinner"><i class="glyphicon glyphicon-refresh"></i></span> '
                                    + buttontext + ' <span class="caret"></span>');
                                schedButton.dropdown("toggle");

                            });
                        return false; });
                    return false;

                }
            })
        }
    });
};


$(document).ready( function() {
        if($(document.body).data('page') == 'specific'){
            pageSpecific();
        }
    }
);

