/**
 * Created by orchie on 11/25/14.
 */

var map;
var currentRoutePath;
var routeCoordinates = [];
var completePath = [];
var directions= {};
var direction1 = [];
var direction2 =[];




function showBusRoute(routeTag,link) {

   // var routeUrl = link;

   // console.log("RouteTag:", routeTag);
   // console.log("Link:", link);
   // console.log("routeUrl", routeUrl);


   // get route data to on the map
    $.ajax({
        type: 'GET',
        url: link,
        beforeSend: function (){ $('#routes').toggleClass('active');},
        complete: function() { $('#routes').toggleClass('active');},

        success: function (points) {

            //Clear old path
            if(currentRoutePath) {
                var arrayLength = completePath.length;
                for( var i =0; i < arrayLength; i++){
                    completePath[i].setMap(null);
                }
                completePath = [];

            }

            //Get the route color
            var routeCol = '#' + points.routeColor; // color suggested by actransit is too pale make all red
            var latmin = points.routeLatMin;
            var latmax = points.routeLatMax;
            var lonmin = points.routeLonMin;
            var lonmax = points.routeLonMax;

            //Get the route path data points
            $(points.routeSegments).each(function (i, item) {
                $(item.routePoints).each(function (j, point) {

                    var lat = point.lat
                    var lon = point.lon

                    if (typeof(lat) === "undefined" || typeof(lon) === "undefined")
                        console.log("does not exist");
                    else {
                        routeCoordinates.push(new google.maps.LatLng(lat, lon));
                    }

                });
                currentRoutePath = new google.maps.Polyline({
                    path: routeCoordinates,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                //addline()
                currentRoutePath.setMap(map);
                completePath.push(currentRoutePath)
                routeCoordinates = [];
            });


            //Get the station data for each direction
            $(points.routeDirections).each( function(k, direc) {

                if (k == 0) {
                    directions['name1'] = direc.directionName;
                    directions['direction1'] = direc.directionName;
                    $('a#direction1').text(direc.directionName);
                } else {
                    directions['name2'] = direc.directionName;
                    directions['direction2'] = direc.directionName;
                    $('a#direction2').text(direc.directionName);
                }

                //Add the marker position to each direction
                $(direc.directionStops).each(function (l, stop) {

                    var stopMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(stop.lat, stop.lon),
                        map: null,
                        title: stop.stopTitle
                    // icon: image
                    });

                    var stopMarkerInfo = new google.maps.InfoWindow();
                    google.maps.event.addListener(stopMarker,'mouseout', function(e){

                       if(stopMarkerInfo){
                           stopMarkerInfo.close();
                       }
                    });
                    //stopMarkerInfo.open(map,stopMarker);
                    google.maps.event.addListener(stopMarker,'dblclick', function() {

                        $.post( "updateWatch",{
                                           agency: "actransit",
                                           route: points.routeTag,
                                           title: stop.stopTitle,
                                           direction: direc.directionName,
                                           stop: stop.stopTag}, function(resp) {

                                                                if( resp.updateresponse.response === "ok"){
                                                                    var newDiv = document.createElement('div');
                                                                    newDiv.innerHTML = '<p>Stop'
                                                                        + stop.stopTitle +' Saved </p>';

                                                                   stopMarkerInfo.setContent(newDiv);
                                                                   stopMarkerInfo.open(map,stopMarker);
                                                                }else{
                                                                    var newDiv = document.createElement('div');
                                                                    newDiv.innerHTML = '<p>' +resp.updateresponse.response
                                                                        + '</p>';
                                                                    stopMarkerInfo.setContent(newDiv);
                                                                    stopMarkerInfo.open(map,stopMarker);

                                                                }
                                             });

                    });

                    google.maps.event.addListener(stopMarker,'click',function(){
                        //stopMarkerInfo.open(map,stopMarker);
                    });

                    //var stopMarker = new google.maps.LatLng(stop.lat, stop.lon);
                    if (k == 0) {
                        direction1.push(stopMarker);
                     } else {
                        direction2.push(stopMarker);
                    }

                });
            });
            $('#directionButtons a').show();
             directions['data1'] = direction1;
             directions['data2'] = direction2;

            var sw = new google.maps.LatLng(latmin, lonmin);
            var ne = new google.maps.LatLng(latmax, lonmax);
            var bounds = new google.maps.LatLngBounds(sw, ne);
            map.fitBounds(bounds);

           // console.log("path", currentRoutePath);
           // console.log("map", map);
        }
    });
    //return false;

}

function setAllDirectionMarkers(map,direct) {

    var dataChoice;
    if(direct == 'All')
        dataChoice = direction1.concat(direction2);
    if(direct == 'direction1') {
        //  $('a#direction1').text(direct);
        dataChoice = direction1;
    }
    if(direct == 'direction2'){
      //  $('a#direction2').text(direct)
        dataChoice = direction2;
    }

    if(dataChoice) {
       // $('#directionButtons a').show();
       // $('a#direction1').text(direct);

        for (var i = 0; i < dataChoice.length; i++) {
            dataChoice[i].setMap(map);
        }
    }
}

function clearAllMarkers() {
   // setAllDirectionMarkers(null,'All');
    for (var i = 0; i < direction1.length; i++) {
        direction1[i].setMap(null);
    }

    for (var i = 0; i < direction2.length; i++) {
        direction2[i].setMap(null);
    }
}

function deleteMarkers() {
    clearAllMarkers();
    directions = [];
    direction1 = [];
    direction2 = [];
}

function clearLine(){
    currentRoutePath.setMap(null);
    currentRoutePath = null;
}

function addLine() {
    currentRoutePath.setMap(map);
}


$(document).ready( function() {



        if ($(document.body).data('page') == 'transitmap') {

            console.log("Page transitmap");

            function initialize() {
                var mapOptions = {
                    center: {lat: 37.8044, lng: -122.2708},
                    zoom: 12

                };

                map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

            }

            initialize();
            $('#directionButtons a').hide();
            $('#mylist a').click(function () {
                    $('#routes').dropdown("toggle");
                }
            );

            $('#mylist a').on('click', function () {
                deleteMarkers();
                showBusRoute(this.id, this.href);
                return false;
            })
            $('#directionButtons a').on('click', function () {
                clearAllMarkers();
                setAllDirectionMarkers(map, this.id);
                return false;
            });

            $('[id^=marker] a').click(function (minfo) {
                console.log("Info Button Pressed", this.id)
            });


        }
    }
);