class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller:"route", action: "index")
        "/updateWatch" {
            controller = "route"
            action = 'updateWatch'
        }

        "500"(view:'/error')
	}
}
