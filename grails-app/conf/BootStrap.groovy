//package edu.harvard.cscie56.finalproject

import edu.harvard.cscie56.finalproject.BusRole
import edu.harvard.cscie56.finalproject.BusUser
import edu.harvard.cscie56.finalproject.BusUserProfile
import edu.harvard.cscie56.finalproject.BusUserBusRole
import edu.harvard.cscie56.finalproject.RoutePoint
import edu.harvard.cscie56.finalproject.RouteSegment
import edu.harvard.cscie56.finalproject.Route
import edu.harvard.cscie56.finalproject.Direction
import edu.harvard.cscie56.finalproject.Stop
import edu.harvard.cscie56.finalproject.ScheduleStop
import edu.harvard.cscie56.finalproject.Schedule
import edu.harvard.cscie56.finalproject.ScheduleTime
import edu.harvard.cscie56.finalproject.WatchStop
import org.joda.time.*


import grails.converters.JSON

class BootStrap {

    def init = { servletContext ->




        JSON.registerObjectMarshaller(RouteSegment) { RouteSegment rs ->
            return [id           : rs.id,
                    segmentNumber: rs.segmentNumber,
                    routePoints  : rs.routePoints.sort { it.seqNum }.collect {
                        return [seqNum: it.seqNum, lat: it.lat, lon: it.lng]
                    }]
        }

        JSON.registerObjectMarshaller(Direction) { Direction d ->
            return [
                    directionName : d.directionName,
                    directionTag  : d.directionTag,
                    directionStops: d.stops.collect { return [ stopTag: it.stopTag, stopTitle: it.stopTitle, lat: it.lat, lon: it.lng] }]

        }

        JSON.registerObjectMarshaller(Route) { Route r ->
            return [
                    routeTag     : r.routeTag,
                    routeTitle   : r.routeTitle,
                    routeColor   : r.routeColor,
                    routeLatMax  : r.routeLatMax,
                    routeLatMin  : r.routeLatMin,
                    routeLonMax  : r.routeLngMax,
                    routeLonMin  : r.routeLngMin,
                    routeSegments: r.routeSegments,
                    routeDirections: r.directions

            ]
        }

        JSON.registerObjectMarshaller(Schedule) { Schedule s ->
            return [id           : s.id,
                    routeTag     : s.scheduleRoute,
                    scheduleStops: s.scheduleStops
            ]
        }

        JSON.registerObjectMarshaller(ScheduleStop) { ScheduleStop ss ->
            return [id           : ss.id,
                    stopTag      : ss.schedStopTag,
                    stopTitle    : ss.schedStopTitle,
                    direction    : ss.schedDirection,
                    scheduleTimes: ss.schedDepTimes.sort{it.schedTime}]
        }

        JSON.registerObjectMarshaller(ScheduleTime) { ScheduleTime st ->
            return [time: st.getDisplayString(), timeinmillis: st.getTimeInMillis()]
        }


        environments {
            development {
                //println "Creating sample data"
               // createSampleData()
            }
            test {
                //println "Creating sample data"
                // if (!RouteSegment.count()) createSampleData()
                //  createSampleData()
            }
        }

    }


    private createSampleData() {
        println "Creating sample data"

        def adminRole = new BusRole(authority: 'ROLE_ADMIN').save(flush: true)
        def userRole = new BusRole(authority: 'ROLE_USER').save(flush: true)

        def testUser = new BusUser(username: 'me', password: 'password')
        testUser.save(flush: true)

        //BusUserBusRole.create testUser, adminRole, true

        def now = new Date()
        def chuck = new BusUser(
                username: "chuck_norris",
                password: "highkick",
                profile: new BusUserProfile(fullName: "Chuck Norris", email: "alan.orcharton@gmail.com")
        ).save(failOnError: true, flush: true)

        BusUserBusRole.create testUser, adminRole, true
        BusUserBusRole.create chuck, userRole, true
        //stopTag: "1015030",

        WatchStop wstop1 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag: "NL", stopTag: "9902310",
                stopTitle: "Macarthur Blvd & Lake Shore Av", stopDirection: "west", preemptMins: new Minutes(5),
                agency: "actransit", schedTime: new LocalTime(9, 35, 00), preemptTime: new LocalTime(16, 10, 00).getMillisOfDay())

        WatchStop wstop2 = new WatchStop(notifyHandle: "@bottomsup", notifyType: "twitter", routeTag: "B", stopTag: "1014240",
                stopTitle: "Longridge Rd & Grosvenor Pl", stopDirection: "west", preemptMins: new Minutes(5),
                schedTime: new LocalTime(18, 35, 00), agency: "actransit", preemptTime: new LocalTime(17, 30, 00).getMillisOfDay())

        wstop1.save(failOnError: true)
        wstop2.save(failOnError: true)

        chuck.addToStopsWatched(wstop1)
        chuck.addToStopsWatched(wstop2)
        chuck.save(failOnError: true, flush: true)
    }
}







