<%--
  Created by IntelliJ IDEA.
  User: orchie
  Date: 12/2/14
  Time: 3:35 AM
--%>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
    <style>
        .row-centered {
            text-align:center;
        }
        .col-centered {
            display:inline-block;
            float:none;
             /* reset the text-align */
            text-align:left;
            /* inline-block space fix */
            margin-right:-4px;
        }
    </style>

</head>

<body>
    <div class="container"></div>
        <div class="row row-centered">
            <div class="col-md-6 col-centered col-max">
                <div class="well">
                     <h1>Register New User</h1>
                     <g:hasErrors>
                        <div class="errors">
                        <g:renderErrors bean="${user}" as="list" />
                        </div>
                    </g:hasErrors>
                <g:if test="${flash.message}">
                    <div class="flash">${flash.message}</div>
                </g:if>
                <form action="register" method="POST">
                <fieldset>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" class="form-control" name="username" value="${user?.username}">
                    </div>
                    <div class="form-group has-success">
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" class="form-control" >
                    </div>
                    <div class="form-group has-success">
                        <label for="profile.fullName">Full name</label>
                        <input type="text" id="profile.fullName" name="profile.fullName" class="form-control" value="${user?.profile?.fullName}">
                    </div>
                        <div class="form-group has-success">
                    <div class="fieldcontain required">
                         <label for="profile.email">Email</label>
                        <input type="text" id="profile.email" name="profile.email" class="form-control" value="${user?.profile?.email}">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Register</button>
                </fieldset>
                </form>

            </div>
        </div>
    </div>
</div>
</body>

</html>
