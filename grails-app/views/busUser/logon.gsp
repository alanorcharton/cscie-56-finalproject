<%--
  Created by IntelliJ IDEA.
  User: orchie
  Date: 12/4/14
  Time: 10:43 PM
--%>
<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>

    <meta name="layout" content="main" />
</head>

<body>
<div class="container"></div>
<div class="row row-centered">
    <div class="col-md-6 col-centered col-max">
        <div class="well">
            <h1>Login</h1>
            <g:hasErrors>
                <div class="errors">
                    <g:renderErrors bean="${user}" as="list" />

                </div>
            </g:hasErrors>
            <g:if test="${flash.message}">
                <div class="flash">${flash.message}</div>
            </g:if>
            <g:form uri="/j_spring_security_check" method="POST">
                <fieldset>
                    <div class="form-group">
                        <label for="j_username">Username</label>
                        <input type="text" id="j_username" class="form-control" name="j_username" value="${username}">
                    </div>
                    <div class="form-group has-success">
                        <label for="j_password">Password</label>
                        <input type="password" id="j_password" name="j_password" class="form-control" >
                    </div>

                    <div class="checkbox">
                        <label for="j_spring_security_remember_me">
                            <input type="checkbox" id="j_spring_security_remember_me" name="j_spring_security_remember_me">Remember me
                        </label>

                    </div>
                    <button type="submit" class="btn btn-primary">Login</button>
                    <a href="../busUser/register" class="btn btn-success">Register</a>
                </fieldset>
            </g:form>

        </div>
    </div>
</div>
</div>
</body>
</html>
