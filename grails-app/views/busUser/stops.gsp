<%--
  Created by IntelliJ IDEA.
  User: orchie
  Date: 12/2/14
  Time: 11:56 PM
--%>



<html xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="main" />
</head>

<body data-page="specific">
<div class="container">

    <div class="panel panel-warning">
        <div class="panel-heading text-center">${user?.username}'s Monitored Stops</div>
        <div class="panel-body">
            Notification Address is : <b> ${user?.profile.email} </b>
        </div>
    </div>
</div>
<div class="container">
    <div class="well">
        <table class="table table-striped">
            <tr>
                <td><b><g:message code="watchStop.routeTag.name"/></b></td>
                <td><b>Stop Name</b></td>
                <td><b>Direction</b></td>
                <td><b>Schedule Alerts</b></td>
                <td><b>Alert Mins prior</b></td>
                <td><b>Delete</b></td>

            </tr>
            <g:each var="stop" in="${user?.stopsWatched}">
            <tr>
                <td>${stop.routeTag}</td>
                <td>${stop.stopTitle}</td>
                <td>${stop.stopDirection}</td>
                <td>
                    <div id="watchedStop-${stop.id}" class="dropdown">
                        <a id="schedButton-${stop.id}" href="../route/getSchedule/${stop.id}"  class="btn btn-default dropdown-toggle has-spinner" type="button" data-toggle="dropdown">
                            <span id="spinner-${stop.id}" class="spinner"><i class="glyphicon glyphicon-refresh"></i></span> ${stop.schedTime?.toString("HH:mm") ?: "No Schedule "} <span class="caret"></span>
                        </a>
                        <ul id="schedList-${stop.id}" class="dropdown-menu">

                        </ul>
                    </div>
                </td>
                <td>
                    <div id="watchedMins-${stop.id}" class="dropdown">
                        <a id="minsButton-${stop.id}" href="#" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                             ${stop.preemptMins?.getMinutes()} Mins <span class="caret"></span>
                        </a>
                        <ul id="minsList-${stop.id}" class="dropdown-menu">
                            <g:each var="minsTime" in="${preemptOptions}">
                            <li><a id="minItem-${minsTime}-${stop.id}" href="../busUser/updatePreemptMins/${minsTime}">${minsTime}</a></li>
                            </g:each>
                        </ul>
                    </div>
                </td>
                <td>
                    <a id="deleteButton-${stop.id}" href="deleteWatchRecord" class="btn btn-danger" type="button">
                        <span class="glyphicon glyphicon-remove-circle"></span>
                    </a>
                </td>
            </tr>
            </g:each>
        </table>
    </div>
</div>
</body>
</html>

