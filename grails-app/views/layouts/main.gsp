<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <r:require modules="bootstrap"/>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="spinner.css"/>
    <asset:stylesheet src="userforms.css" />
    <asset:stylesheet src="schedule.css" />
    <asset:javascript src="application.js"/>
    <asset:javascript src="transitmap.js"/>
    <g:layoutTitle/>
</head>
<body data-page="${pageProperty(name: 'body.data-page')}">
<div class="navbar navbar-default navbar-inverse navbar-static-top">
    <div class="container">
        <div class = "navbar-header">
            <a href="#" class="navbar-brand"><b>LastBus!</b> <sub>AC Transit</sub></a>
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse navHeaderCollapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${createLink (controller: 'route', action: 'index')}" type="button" class="btn btn-dark ">Home</a> </li>
                <li class="dropdown">
                    <a id="routes" href="${createLink (controller: 'route', action: 'index')}" class="btn btn-dark dropdown-toggle has-spinner" data-toggle="dropdown" type="button">
                        <span id="spinner-routes" class="spinner"><i class="glyphicon glyphicon-refresh"></i></span> Routes <b class="caret"></b></a>

                    <ul class="dropdown-menu" id='mylist'>
                        <g:each var="route" in="${routes}">
                            <li><a href="${createLink (controller: 'route', action:'loadRoutes', id:"${route.routeTag}")}">${route.routeTag}</a></li>

                        </g:each>

                    </ul>

                </li>
                <sec:ifNotLoggedIn>
                <li>

                    <g:form name="loginForm" controller="busUser" action="logon">
                        <button type="submit" class="btn btn-dark expand">Login</button>
                    </g:form>
                </li>
                </sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                 <li>
                        <a href="${createLink (controller: 'busUser', action: 'stops')}" class="btn btn-dark expand" type="button">My Stops</a>
                 </li>
                 <li>
                     <g:form name="logoutForm" controller="logout" action="index">
                         <button type="submit" class="btn btn-dark expand">Logout</button>
                     </g:form>
                 </li>

                </sec:ifLoggedIn>
                <li>

                <div class="btn-group" role="toolbar" aria-label="..." id="directionButtons">
                    <a type="button" class="btn btn-default" href="#" id="direction1">dir1</a>
                    <a type="button" class="btn btn-default" href="#" id="direction2">dir2</a>
                </div>
                </li>

            </ul>

        </div>
    </div>
</div>
<g:layoutBody/>


</body>
</html>

