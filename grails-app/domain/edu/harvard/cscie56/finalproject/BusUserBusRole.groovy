package edu.harvard.cscie56.finalproject

import org.apache.commons.lang.builder.HashCodeBuilder

class BusUserBusRole implements Serializable {

    private static final long serialVersionUID = 1

    BusUser busUser
    BusRole busRole

    boolean equals(other) {
        if (!(other instanceof BusUserBusRole)) {
            return false
        }

        other.busUser?.id == busUser?.id &&
                other.busRole?.id == busRole?.id
    }

    int hashCode() {
        def builder = new HashCodeBuilder()
        if (busUser) builder.append(busUser.id)
        if (busRole) builder.append(busRole.id)
        builder.toHashCode()
    }

    static BusUserBusRole get(long busUserId, long busRoleId) {
        BusUserBusRole.where {
            busUser == BusUser.load(busUserId) &&
                    busRole == BusRole.load(busRoleId)
        }.get()
    }

    static boolean exists(long busUserId, long busRoleId) {
        BusUserBusRole.where {
            busUser == BusUser.load(busUserId) &&
                    busRole == BusRole.load(busRoleId)
        }.count() > 0
    }

    static BusUserBusRole create(BusUser busUser, BusRole busRole, boolean flush = false) {
        def instance = new BusUserBusRole(busUser: busUser, busRole: busRole)
        instance.save(flush: flush, insert: true)
        instance
    }

    static boolean remove(BusUser u, BusRole r, boolean flush = false) {
        if (u == null || r == null) return false

        int rowCount = BusUserBusRole.where {
            busUser == BusUser.load(u.id) &&
                    busRole == BusRole.load(r.id)
        }.deleteAll()

        if (flush) {
            BusUserBusRole.withSession { it.flush() }
        }

        rowCount > 0
    }

    static void removeAll(BusUser u, boolean flush = false) {
        if (u == null) return

        BusUserBusRole.where {
            busUser == BusUser.load(u.id)
        }.deleteAll()

        if (flush) {
            BusUserBusRole.withSession { it.flush() }
        }
    }

    static void removeAll(BusRole r, boolean flush = false) {
        if (r == null) return

        BusUserBusRole.where {
            busRole == BusRole.load(r.id)
        }.deleteAll()

        if (flush) {
            BusUserBusRole.withSession { it.flush() }
        }
    }

    static constraints = {
        busRole validator: { BusRole r, BusUserBusRole ur ->
            if (ur.busUser == null) return
            boolean existing = false
            BusUserBusRole.withNewSession {
                existing = BusUserBusRole.exists(ur.busUser.id, r.id)
            }
            if (existing) {
                return 'userRole.exists'
            }
        }
    }

    static mapping = {
        id composite: ['busRole', 'busUser']
        version false
    }
}
