package edu.harvard.cscie56.finalproject

class Stop {

    Date dateCreated
    String stopTag
    String stopId
    String stopTitle
    String lat
    String lng



    static constraints = {
        stopId nullable: true
    }
}
