package edu.harvard.cscie56.finalproject

class BusRole {

    String authority

    static mapping = {
        cache true
    }

    static constraints = {
        authority blank: false, unique: true
    }
}
