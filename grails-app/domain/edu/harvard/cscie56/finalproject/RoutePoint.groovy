package edu.harvard.cscie56.finalproject

class RoutePoint {

    Integer seqNum
    String lat
    String lng

    static constraints = {
        routeSegment nullable: true
        seqNum nullable: false
        lat nullable: false, blank: false
        lng nullable: false, blank: false

    }

    static belongsTo = [ routeSegment : RouteSegment]
}
