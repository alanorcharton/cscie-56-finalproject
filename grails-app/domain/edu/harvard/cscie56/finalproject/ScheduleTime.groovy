package edu.harvard.cscie56.finalproject

import org.jadira.usertype.dateandtime.joda.PersistentLocalTime
import org.joda.time.LocalTime

class ScheduleTime {

    ScheduleStop stop
    LocalTime schedTime

    static belongsTo = [stop : ScheduleStop]

    static constraints = {
        stop nullable: true
        schedTime type: PersistentLocalTime, nullable: false
    }

    String toString() {return "${schedTime?.toString("HH:mm")}"}
    String getDisplayString() { return "${schedTime?.toString("HH:mm")}"}
    String getTimeInMillis() { return "${schedTime?.getMillisOfDay()}"}

    //getMillisOfDay()
}
