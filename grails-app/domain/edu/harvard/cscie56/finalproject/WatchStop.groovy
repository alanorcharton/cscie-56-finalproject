package edu.harvard.cscie56.finalproject

import org.joda.time.*
import org.jadira.usertype.dateandtime.joda.*

class WatchStop {

    BusUser user
    String notifyHandle
    String notifyType
    String routeTag
    String stopTag
    String stopTitle
    String stopDirection
    Minutes preemptMins
    LocalTime schedTime
    LocalTime notifyTime
    String agency
    Integer preemptTime

    static belongsTo = [user : BusUser]


    static constraints = {
        user nullable: true
        notifyHandle blank: false
        notifyType blank: true
        routeTag blank: false
        stopTag blank: false
        stopDirection blank: false
        preemptMins type:PersistentMinutes, nullable: true
        schedTime type: PersistentLocalTime, nullable: true
        notifyTime type: PersistentLocalTime, nullable: true
        agency blank: false
        schedTime type: PersistentLocalTime, nullable: true
        preemptTime type: PersistentLocalTime, nullable: true
        stopTitle blank: true


    }

    String toMultiPredictString() { return "${routeTag}|${stopTag}"}


}
