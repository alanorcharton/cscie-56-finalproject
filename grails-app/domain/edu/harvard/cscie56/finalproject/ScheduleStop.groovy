package edu.harvard.cscie56.finalproject

import org.jadira.usertype.dateandtime.joda.PersistentLocalTime

class ScheduleStop {

    Schedule parentSchedule
    String schedRouteTag
    String schedStopTag
    String schedStopTitle
    String schedDirection

    static hasOne = [parentSchedule: Schedule]
    static hasMany = [schedDepTimes : ScheduleTime]

    static constraints = {
        parentSchedule nullable: true
        schedDirection nullable: false, blank: false
        schedStopTag   nullable: false, blank: false
        schedStopTitle nullable: true, blank: false
        schedRouteTag  nullable: false, blank: false
        schedDepTimes  nullable: true

    }


    static mapping = {
        schedDirection column: '`scheddir`'
    }


    def beforeInsert() {
        convertDirection()
    }

    def beforeUpdate() {
        if (isDirty('scheddir')) {
            convertDirection()
        }
    }

    protected void convertDirection() {
        schedDirection = schedDirection ? schedDirection.toLowerCase() : schedDirection
    }
}
