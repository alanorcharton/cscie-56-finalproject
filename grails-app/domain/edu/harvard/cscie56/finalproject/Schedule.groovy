package edu.harvard.cscie56.finalproject

class Schedule {

    Date dateCreated
    String scheduleRoute
    String serviceClass
    String scheduleTitle
    String scheduleDirection



    static hasMany = [scheduleStops: ScheduleStop]

    static constraints = {
        scheduleRoute nullable: false, blank: false
        scheduleTitle nullable: false, blank: false
        serviceClass nullable: false, blank: true
        scheduleDirection nullable: false, blank: true
    }


    static mapping = {
        scheduleDirection column: '`scheduledirection`'
    }


    def beforeInsert() {
        convertDirection()
    }

    def beforeUpdate() {
        if (isDirty('scheduledirection')) {
            convertDirection()
        }
    }

    protected void convertDirection() {
        scheduleDirection = scheduleDirection ? scheduleDirection.toLowerCase() : scheduleDirection
    }


}
