package edu.harvard.cscie56.finalproject

class Route {

    Date dateCreated
    String routeTag
    String routeTitle
    String routeColor
    String routeLatMin
    String routeLatMax
    String routeLngMin
    String routeLngMax
    String agency



    static constraints = {
        routeTag nullable: false, blank: false
        routeTitle nullable: false, blank: false
        routeColor nullable: true, blank: true
        routeLatMin nullable: true, blank: true
        routeLatMax nullable: true, blank: true
        routeLngMin nullable: true, blank: true
        routeLngMax nullable: true, blank: true
        agency nullable: true, blank: true
        routeSegments nullable:true
        directions nullable: true
        stops nullable: true
    }

    static hasMany = [ routeSegments : RouteSegment, directions: Direction, stops: Stop]
}
