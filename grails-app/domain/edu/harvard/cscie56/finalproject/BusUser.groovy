package edu.harvard.cscie56.finalproject

class BusUser {

    transient springSecurityService

    String username
    String password

    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static transients = ['springSecurityService']

    static hasOne = [profile : BusUserProfile]
    static hasMany = [stopsWatched : WatchStop]



    static constraints = {
        username blank: false, nullable: false, unique: true
        password blank: false, minSize: 4, validator: { password, user ->
           if (password == user.username) return ['busUser.password.custom.error'] }
        profile nullable: true
        stopsWatched nullable: true

    }

    static mapping = {
        password column: '`password`'
    }

    Set<BusRole> getAuthorities() {
        BusUserBusRole.findAllByBusUser(this).collect { it.busRole }
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }


}
