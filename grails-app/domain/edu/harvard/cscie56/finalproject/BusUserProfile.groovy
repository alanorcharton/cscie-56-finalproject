package edu.harvard.cscie56.finalproject

import org.hibernate.validator.constraints.Email

class BusUserProfile {

    BusUser user
    String email
    String twitterHandle
    String fullName

    static belongsTo = [user : BusUser]


    static constraints = {

        fullName blank: false
        email Email: true
        twitterHandle blank: true, nullable: true

    }
}
