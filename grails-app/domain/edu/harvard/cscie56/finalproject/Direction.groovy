package edu.harvard.cscie56.finalproject

class Direction {

    Date dateCreated
    String directionTag
    String directionTitle
    String directionName

    static hasMany = [stops : Stop]
    static belongsTo = [parentRoute : Route]

    static constraints = {
        stops nullable: true
        parentRoute nullable: true
    }
}
