package edu.harvard.cscie56.finalproject

class RouteSegment {

    Date dateCreated
    Integer segmentNumber

    static constraints = {
        route nullable: true
    }
    static hasMany = [ routePoints : RoutePoint]
    static belongsTo = [ route : Route]
}
