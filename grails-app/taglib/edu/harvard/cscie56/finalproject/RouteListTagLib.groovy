package edu.harvard.cscie56.finalproject

import grails.plugins.rest.client.RestBuilder

class RouteListTagLib {
    //static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def routeList = { attrs, body ->
           def restBuilder = new RestBuilder()
           def url = "http://localhost:8080/FinalProject/routeListRest/index"
           def response = restBuilder.get(url)
           def json = response.json
           def myRoutes = []
           def routes = json.each{ r ->
               myRoutes << "${r.title}"
           }

          // out << "<li>< a h"
           out << myRoutes.collect {}
           out << "<li></li>"
    }
}
