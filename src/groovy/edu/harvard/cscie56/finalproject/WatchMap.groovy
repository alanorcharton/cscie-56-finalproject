package edu.harvard.cscie56.finalproject

/**
 * Created by orchie on 12/13/14.
 */
class WatchMap {

    Map<String,List<WatchStop>> watchStopsMap
    Integer stopCount =0

    WatchMap(){
        watchStopsMap = [:]
    }

    def addAgencyStops(List<WatchStop> watchStops){

        for( stop in watchStops){
            String mapKey = "${stop.agency}"
            addStopsToMap(mapKey,stop)
        }

    }

    private void addStopsToMap(String mapKey, WatchStop watchStop){

        if(mapKey) {
            if (!watchStopsMap[mapKey]) {
                    def stopList = []
                    stopList << watchStop
                    watchStopsMap[mapKey] = stopList
                    stopCount++
            } else {
                    watchStopsMap[mapKey] << watchStop
                    stopCount++
            }
        }
    }

    def addWatchStops(List<WatchStop> watchStops){

        for( stop in watchStops){
            String mapKey = "${stop.routeTag}|${stop.stopTag}"
            addStopsToMap(mapKey,stop)
        }
    }
    /*
    def addWatchStops(List<WatchStop> watchStops){

        for( stop in watchStops){
           String mapKey = "${stop.routeTag}|${stop.stopTag}"
           if(!watchStopsMap[mapKey]){
               def stopList = []
               stopList << stop
               watchStopsMap[mapKey] = stopList
               stopCount++

           } else{
               watchStopsMap[mapKey] << stop
               stopCount++
           }

        }
    }
    */

    List<WatchStop> getWatchStops(String predictionKey){

        return watchStopsMap[predictionKey]

    }

    Set<String> getPredictionKeys(){
        return watchStopsMap.keySet()
    }

}
