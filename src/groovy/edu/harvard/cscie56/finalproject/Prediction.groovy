package edu.harvard.cscie56.finalproject

/**
 * Created by orchie on 12/13/14.
 */
class Prediction {

    String agencyTitle
    String routeTag
    String stopTag
    String stopTitle
    String directionTitle
    List<String> predictions = []
    List<String> emails = []


    String toMessage() {

        StringBuilder times = new StringBuilder()
        predictions.each{ it ->

           times <<  "${it} Minutes\n"
        }

        "Predictions for ${agencyTitle}, route ${routeTag}, stop ${stopTitle}\n" +
        "Direction : ${directionTitle}\n" + times

    }

    String getPredictionKey(){
        return "${routeTag}|${stopTag}"
    }

}
